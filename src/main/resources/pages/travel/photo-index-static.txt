cq:template,,/apps/travelcom/templates/listPage
jcr:mixinTypes,,mix:referenceable, mix:versionable
jcr:primaryType,,cq:PageContent
sling:resourceType,,travelcom/components/pages/listPage
doNotFeature,,true
doNotSearch,,true
jcr:primaryType,listType,nt:unstructured
lists,listType,listPhotoGallery
sling:resourceType,listType,travelcom/components/list/listType
hubAnchorTitle,listType/listPhotoGallery,${hubAnchorTitle} Galleries
jcr:primaryType,listType/listPhotoGallery,nt:unstructured
sling:resourceType,listType/listPhotoGallery,travelcom/components/list/listPhotoGallery
jcr:primaryType,assetTitle,nt:unstructured
sling:resourceType,assetTitle,travelcom/components/article/assetTitle
jcr:primaryType,assetDescription,nt:unstructured
sling:resourceType,assetDescription,travelcom/components/article/assetDescription
jcr:primaryType,image,nt:unstructured
package main.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.jcr.RepositoryException;

public final class PathConverter {
	
	private PathConverter() {
	}
	
	public static String getConvertedEpisodePath(String oldPath) {
		return getConvertedEpisodeOrSeriesPath(oldPath, 3);
	}

	public static String getConvertedSeriesPath(String oldPath) {
		return getConvertedEpisodeOrSeriesPath(oldPath, 2);
	}	
	
	private static String getConvertedEpisodeOrSeriesPath(String oldPath, int episodesFolderIndex) {
		String path = oldPath.replace(Config.getGoldPath(), Config.getTestPath());
		
		List<String> pathList = new LinkedList<String>();
		pathList.addAll(Arrays.asList(path.split("/")));
		pathList.add(pathList.size() - episodesFolderIndex, "episodes");
		
		String result = "";
		for (String tmp : pathList) {
			result += tmp + "/";
		}
		
		return result;
	}
	
	public static String getFNConvertedSingleVideoPath(String oldPath) {
		
		List<String> tmpName = new ArrayList<String>();
		tmpName.addAll(Arrays.asList(oldPath.replace("/jcr:content", "").split("/")));
		String title = tmpName.get(tmpName.size() - 1);
		
		List<String> tmpId = new ArrayList<String>();
		tmpId.addAll(Arrays.asList(title.split("-")));
		String id = tmpId.get(tmpId.size() - 1);
		
		String result = "";
		if (oldPath.startsWith(Config.getGoldPath() + "videos/sc/")) {
			result = Config.getTestPath() + "secured-videos";
		} else {
			result = Config.getTestPath() + "videos";
		}
		
		for (int i=1; i<=4; i++) {
			result += "/" + id.substring(0, i);
		}
		result += "/" + title;
		
		return result;
	}
	
	public static String getCCTVConvertedSingleVideoPath(String oldPath) {
		
		List<String> tmpName = new ArrayList<String>();
		tmpName.addAll(Arrays.asList(oldPath.replace("/jcr:content", "").split("/")));
		String title = tmpName.get(tmpName.size() - 1);
		
		String id = "";
		
		try {
			String assetPath = Sessions.getGoldSession().getNode(oldPath)
					.getProperty("sni:assetLink").getString();
			id = Sessions.getGoldSession().getNode(assetPath).getNode("jcr:content")
					.getProperty("sni:sourceId").getString();
		} catch (RepositoryException e) {
			e.printStackTrace();
			return null;
		}
		
		String result = "";
		if (oldPath.startsWith(Config.getGoldPath() + "videos/sc/")) {
			result = Config.getTestPath() + "secured-videos";
		} else {
			result = Config.getTestPath() + "videos";
		}
		
		for (int i=1; i<=4; i++) {
			result += "/" + id.substring(0, i);
		}
		result += "/" + title + "-" + id;
		
		return result;
	}
	
	public static String getConvertedPath(String oldPath) {
		
		if (oldPath.endsWith("-series/") || oldPath.endsWith("-series")) {
			return getConvertedSeriesPath(oldPath);
		} else if (oldPath.contains("-series/") && !oldPath.contains("/tccom/") && !oldPath.contains("/travel-com/")) {
			return getConvertedEpisodePath(oldPath);
		} else if (oldPath.matches(".+-[\\d]{5,8}\\/$") ||
				oldPath.matches(".+-[\\d]{5,8}$")) {
			return getFNConvertedSingleVideoPath(oldPath);
		} else if (oldPath.contains("/cook/videos/")) {
			return getCCTVConvertedSingleVideoPath(oldPath);
		} else {
			return oldPath
					.replace(Config.getGoldPath(), Config.getTestPath())
					.replace(".html", "");
		}
	}
}

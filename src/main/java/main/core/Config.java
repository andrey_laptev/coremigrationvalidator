package main.core;

import java.util.Arrays;
import java.util.List;

public final class Config {
	
	private static String GOLD_HOST;
	private static String TEST_HOST;
	private static String GOLD_USER;
	private static String TEST_USER;
	private static String GOLD_PASS;
	private static String TEST_PASS;
	private static String GOLD_PATH;
	private static String TEST_PATH;
	private static String BRANCH;
	private static String OUTPUT_FILE;
	private static String FEED_FILE;
	private static List<String> PAGE_TYPES;
	
	private Config() {
	}
	
	public static String getGoldHost() {
		if (GOLD_HOST == null) {
			GOLD_HOST = System.getProperty("goldHost") != null ?
					System.getProperty("goldHost") :
					   "http://author1.travel-prod.sni.travelchannel.com:4502/crx/server";

		}
		return GOLD_HOST;
	}
	
	public static String getTestHost() {
		if (TEST_HOST == null) {
			TEST_HOST = System.getProperty("testHost") != null ?
					System.getProperty("testHost") :
						//"http://author1.coreprod.sni.travelchannel.com:4502/crx/server";
						//"http://author1.show-n-episode-test.sni.travelchannel.com:4502/crx/server";
						//"http://author1.tag-test.sni.travelchannel.com:4502/crx/server";
						//"http://author1.index-pages.sni.travelchannel.com:4502/crx/server";
						//"http://author.video-test.sni.travelchannel.com/crx/server";
						"http://author.broadcast2.sni.travelchannel.com/crx/server";

		}
		return TEST_HOST;
	}
	
	public static String getGoldUser() {
		if (GOLD_USER == null) {
			GOLD_USER = System.getProperty("goldUser") != null ?
					System.getProperty("goldUser") :
						"tcmigration";
		}
		return GOLD_USER;
	}
	
	public static String getTestUser() {
		if (TEST_USER == null) {
			TEST_USER = System.getProperty("testUser") != null ?
					System.getProperty("testUser") :
						"tcmigration";
		}
		return TEST_USER;
	}
	
	public static String getGoldPass() {
		if (GOLD_PASS == null) {
			GOLD_PASS = System.getProperty("goldPass") != null ?
					System.getProperty("goldPass") :
						"tcmigration";
		}
		return GOLD_PASS;
	}
	
	public static String getTestPass() {
		if (TEST_PASS == null) {
			TEST_PASS = System.getProperty("testPass") != null ?
					System.getProperty("testPass") :
						"tcmigration";
		}
		return TEST_PASS;
	}
	
	public static String getGoldPath() {
		if (GOLD_PATH == null) {
			GOLD_PATH = System.getProperty("goldPath") != null ?
					System.getProperty("goldPath") :
						"/content/tccom/en/";
		}
		return GOLD_PATH;
	}
	
	public static String getTestPath() {
		if (TEST_PATH == null) {
			TEST_PATH = System.getProperty("testPath") != null ?
					System.getProperty("testPath") :
						"/content/travel-com/en/";
		}
		return TEST_PATH;
	}
	
	public static String getBranch() {
		if (BRANCH == null) {
			BRANCH = System.getProperty("branch") != null ?
					System.getProperty("branch") :
						"";
		}
		return BRANCH;
	}
	
	public static String getOutputFilePath() {
		if (OUTPUT_FILE == null) {
			OUTPUT_FILE = System.getProperty("out") != null ?
					System.getProperty("out") :
						"out.csv";
		}
		return OUTPUT_FILE;
	}
	
	public static String getFeedFilePath() {
		if (FEED_FILE == null) {
			FEED_FILE = System.getProperty("feed") != null ?
					System.getProperty("feed") :
						null;
		}
		return FEED_FILE;
	}

	public static boolean isStackCompare() {
		return System.getProperty("isStackCompare") != null ?
				System.getProperty("isStackCompare").equals("true") : false;
	}

	public static boolean isVideo() {
		return System.getProperty("isVideo") != null ?
				System.getProperty("isVideo").equals("true") : false;
	}
	
	public static boolean isValidPage(String pageType) {
		String pageTypes = System.getProperty("pageTypes") != null ?
				System.getProperty("pageTypes") :
					"bio,article,episode-listing,recipe-listing,asset-recipes,"
				+ "photo-gallery,company,talent,show,topic,ulp,menu,"
					+ "structural,video-channel,"
				+ "episode,recipe,series,video,video-player,"
					+ "showPageTier2,showPageTier1,articleIndexPage,articlePage,articleTopNPage,episodeIndexPage,openTemplatePage,"
				+ "episodePage,packagePage,cityPageTier1,cityPageTier2,programSchedulePage,interestPageTier1,interestPageTier2,"
					+ "photoGalleryPage,continentRegionPage,photoIndexPage,profilePage,videoIndexPage,universalLandingPage,showPage"
				+ "videoPlaylistPage,countryStatePage,mediadeskVideo,series,videoCollection";
		if (PAGE_TYPES == null) {
			PAGE_TYPES = Arrays.asList(pageTypes.split(","));
		}
		return PAGE_TYPES.contains(pageType);
	}
}

package main.core;

public final class Global {
	
	private static String currentTestPath = null;
	private static String pageType = null;
	
	private Global() {
	}
	
	public static void setCurrentTestPath(String path) {
		currentTestPath = path;
	}
	
	public static String getCurrentTestPath() {
		return currentTestPath;
	}

	public static void setPageType(String path) {
		pageType = path;
	}
	
	public static String getPageType() {
		return pageType;
	}
}

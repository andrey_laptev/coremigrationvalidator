package main.core;

import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;

import org.apache.jackrabbit.commons.JcrUtils;

public final class Sessions {
	
	private static Session GOLD_SESSION;
	private static Session TEST_SESSION;
	
	private Sessions() {
	}
	
	public static Session getGoldSession() {
		if (GOLD_SESSION == null) {
			try {
				Repository goldRepo = JcrUtils.getRepository(Config.getGoldHost());
				GOLD_SESSION = goldRepo.login(new SimpleCredentials(
						Config.getGoldUser(), Config.getGoldPass().toCharArray()));
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}
		return GOLD_SESSION;
	}
	
	public static Session getTestSession() {
		if (TEST_SESSION == null) {
			try {
				Repository testRepo = JcrUtils.getRepository(Config.getTestHost());
				TEST_SESSION = testRepo.login(new SimpleCredentials(
						Config.getTestUser(), Config.getTestPass().toCharArray()));
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}
		return TEST_SESSION;
	}
}

package main.core;

public class Failure {
	
	private String symptom;
	private String page;
	private String component;
	private String goldPath;
	private String goldProperty;
	private String goldValue;
	private String testPath;
	private String testProperty;
	private String testValue;
	
	public Failure (
			String symptom,
			String page,
			String component,
			String goldPath,
			String goldProperty,
			String goldValue,
			String testPath,
			String testProperty,
			String testValue) {
		this.symptom = symptom;
		this.page = page;
		this.component = component;
		this.goldPath = goldPath;
		this.goldProperty = goldProperty;
		this.goldValue = goldValue;
		this.testPath = testPath;
		this.testProperty = testProperty;
		this.testValue = testValue;
	}
	
	public String getSymptom() {
		return symptom;
	}

	public String getPage() {
		return page;
	}
	
	public String getComponent() {
		return component;
	}
	
	public String getGoldPath() {
		return goldPath;
	}
	
	public String getGoldProperty() {
		return goldProperty;
	}
	
	public String getGoldValue() {
		return goldValue;
	}
	
	public String getTestPath() {
		return testPath;
	}
	
	public String getTestProperty() {
		return testProperty;
	}
	
	public String getTestValue() {
		return testValue;
	}
}

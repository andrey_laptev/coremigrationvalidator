package main;
import main.core.Global;
import main.core.Sessions;
import main.reporter.Reporter;

import javax.jcr.*;

public class SingleNodeValidator {


    public static boolean validateInputContent(String line, String[] input){
        if(input.length < 3 || input[0] == null || input[1] == null || input[2] == null) {
            Reporter.log(
                    "SINGLE NODE INVALID INPUT",
                    "",
                    "SINGLE NODE",
                    "",
                    "",
                    "",
                    "Input String - " + line,
                    "", "");
            return false;
        }
        return true;
    }

    public static void validateSingleNode(String inputNodePath, String propertyName, String value){
        System.out.println("Crawling node - " + inputNodePath +" property - " + propertyName + " value - " + value);
        String actualTestValue = null;
        try{

            Property property = Sessions.getTestSession()
                    .getNode(inputNodePath).getProperty(propertyName);

            if(property.isMultiple()){
                actualTestValue = property.getValues()[0].getString();
            } else{
                actualTestValue = property.getValue().getString();
            }

            if (actualTestValue.contains(value)) {

            } else {
                Reporter.log(
                        "SINGLE NODE VALUE MISMATCH",
                        "",
                        "SINGLE NODE",
                        "",
                        "",
                        "",
                        inputNodePath,
                        propertyName, actualTestValue);
                return;
            }

        } catch(PathNotFoundException ex){
            Reporter.log(
                    "NODE/PROPERTY NOT FOUND",
                    "",
                    "SINGLE NODE",
                    "",
                    "",
                    "",
                    inputNodePath,
                    propertyName, "");
        } catch(RepositoryException e2){
            Reporter.log(
                    "SINGLE NODE MAJOR EXCEPTION ",
                    "",
                    "SINGLE NODE",
                    "",
                    "",
                    "",
                    inputNodePath,
                    propertyName, "");
            return;
        }
    }

    public static void compareNode(Node inputNode){
        String nodePath = "";
        try{
            System.out.println(String.format("validating node %s", inputNode.getPath()));
            nodePath = inputNode.getPath();

            Node testNode = Sessions.getTestSession()
                    .getNode(inputNode.getPath());

            PropertyIterator iterator = inputNode.getProperties();

            while(iterator.hasNext()){
                String propertyName = null;
                String goldValue = null;
                String testValue = null;
                try {
                        Property property = iterator.nextProperty();
                        propertyName = property.getName();
                        Property testProperty = testNode.getProperty(propertyName);

                        if (property.isMultiple()) {
                            goldValue = getValues(property);
                            testValue = getValues(testProperty);
                        } else {
                            goldValue = property.getValue().getString();
                            testValue = testProperty.getValue().getString();
                        }

                        if (!goldValue.equals(testValue)) {
                            Reporter.log(
                                    "VALUE MISMATCH",
                                    Global.getPageType(),
                                    "NODE COMPARE",
                                    inputNode.getPath(),
                                    propertyName,
                                    goldValue,
                                    testNode.getPath(),
                                    propertyName, testValue);
                        }

                }catch(PathNotFoundException e){
                    Reporter.log(
                            "PROPERTY IS MISSING",
                            Global.getPageType(),
                            "NODE COMPARE",
                            inputNode.getPath(),
                            propertyName,
                            "",
                            inputNode.getPath(),
                            propertyName, "");
                    continue;
                }
            }
        } catch(PathNotFoundException ex){
            Reporter.log(
                    "NODE IS MISSING",
                    Global.getPageType(),
                    "NODE COMPARE",
                    nodePath,
                    "",
                    "",
                    nodePath,
                    "", "");

        } catch(Exception ex){
            Reporter.log(
                    "FATAL ERROR",
                    Global.getPageType(),
                    "NODE COMPARE",
                    nodePath,
                    "",
                    "",
                    nodePath,
                    "", "");
        }
    }

    public static String getValues(Property property){
        String values = null;
        try {
            for (Value value : property.getValues()) {
                if (value != null) {
                    values += value.getString() + " ";
                }
            }
        } catch(Exception ex){

        }
        return  values;
    }

}

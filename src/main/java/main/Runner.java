package main;

import main.core.Config;
import main.core.Sessions;
import main.reporter.Reporter;

import javax.jcr.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Runner {
	
	public static void main(String[] args) {
		
		long start = System.currentTimeMillis();
		
		Reporter.log(
				"Symptom",
				"Page",
				"Component",
				"Golden Path",
				"Golden Property",
				"Golden Value",
				"Test Path",
				"Test Property",
				"Test Value");
		
		if (Config.getFeedFilePath() == null) {
			try {
				if(Config.isStackCompare()){
					compareSingleNode(Sessions.getGoldSession().getNode(
							Config.getGoldPath() + Config.getBranch()));
				} else if(Config.isVideo()){
					crawl(Sessions.getGoldSession().getNode(Config.getBranch()));
				} else{
					crawl(Sessions.getGoldSession().getNode(
							Config.getGoldPath() + Config.getBranch()));
				}

			} catch (PathNotFoundException e) {
				e.printStackTrace();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		} else {
			try {
				FileReader fileReader = new FileReader(Config.getFeedFilePath());
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				String line;

				if(Config.getFeedFilePath().contains("single_")){
					while ((line = bufferedReader.readLine()) != null) {
						String [] input = line.split(",");
						if(!SingleNodeValidator.validateInputContent(line,input)){
							continue;
						}

						SingleNodeValidator.validateSingleNode(input[0],input[1],input[2]);
					}

				}	else{

					while ((line = bufferedReader.readLine()) != null) {
						crawl(Sessions.getGoldSession().getNode(line));
					}
				}

				bufferedReader.close();

			} catch (PathNotFoundException e) {
				e.printStackTrace();
			} catch (RepositoryException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		Reporter.log("Golden Server: " + Config.getGoldHost(),
				"", "", "", "", "", "", "", "");
		Reporter.log("Test Server: " + Config.getTestHost(),
				"", "", "", "", "", "", "", "");
		Reporter.log("TIME: " + ((System.currentTimeMillis() - start) / 60000) + " mins",
				"", "", "", "", "", "", "", "");
		
	}

	private static void compareSingleNode(Node node)throws RepositoryException {
		SingleNodeValidator.compareNode(node);
		NodeIterator iterator = node.getNodes();
		while (iterator.hasNext()) {
			compareSingleNode(iterator.nextNode());
		}
	}

	private static void crawl(Node node) throws RepositoryException {
		
		Validatable validatable = getValidatable(node);
		
		if (!(validatable == null && "jcr:content".equals(node.getName())) &&
				!node.getPath().contains("content/food/test/") &&
				!node.getPath().contains("content/food/drafts/") &&
				!node.getPath().contains("/chefs/jcr:content") &&
				!node.getPath().contains("/hosts/jcr:content")) {
			
			if (validatable != null) {
				validatable.validate();
				
				if ("SingleVideo".equals(validatable.getClass().getSimpleName()) ||
						"Recipe".equals(validatable.getClass().getSimpleName())) {
					return;
				}
			}
			
			NodeIterator iterator = node.getNodes();
			while (iterator.hasNext()) {
				crawl(iterator.nextNode());
			}
		}
	}
	
	private static Validatable getValidatable(Node node)
			throws ValueFormatException, RepositoryException {

		System.out.println(String.format("crawling node %s", node.getPath()));

		Validatable validatable;

		validatable = FnRunner.getValidatable(node);
		if(validatable != null) {
			return validatable;
		}

		validatable = CctvRunner.getValidatable(node);
		if(validatable != null) {
			return validatable;
		}

		validatable = TravelRunner.getValidatable(node);
		if(validatable != null) {
			return validatable;
		}

		return null;
	}
}

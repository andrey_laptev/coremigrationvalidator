package main.rules;

public class MappingRule {

	private String goldNode;
	private String goldName;
	private String testNode;
	private String testName;
	
	public MappingRule(String goldName, String goldNode, String testName, String testNode) {
		this.goldNode = goldNode;
		this.goldName = goldName;
		this.testNode = testNode;
		this.testName = testName;
	}
	
	public String getGoldNode() {
		return this.goldNode;
	}
	
	public String getGoldName() {
		return this.goldName;
	}
	
	public String getTestNode() {
		return this.testNode;
	}
	
	public String getTestName() {
		return this.testName;
	}
}

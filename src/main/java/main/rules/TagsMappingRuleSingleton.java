package main.rules;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TagsMappingRuleSingleton {

    private static TagsMappingRuleSingleton instance = null;
    protected static List<TagsMappingRule> tagsMapping = new ArrayList<TagsMappingRule>();

    private TagsMappingRuleSingleton() {
        fillTagsMap("src/main/resources/components/travel/tags.txt");
    }

    public static TagsMappingRuleSingleton getInstance() {
        if(instance == null) {
            instance = new TagsMappingRuleSingleton();
        }
        return instance;
    }

    protected void fillTagsMap(String path) {
        FileReader fileReader;
        try {
            fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if (!line.startsWith("#")) {
                    String[] tmp = line.split(",", -1);
                    if (tmp.length > 2) {
                        tagsMapping.add(new TagsMappingRule(tmp[0], tmp[1], tmp[2]));
                    } else {
                        tagsMapping.add(new TagsMappingRule(tmp[0], tmp[1]));
                    }

                }
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static TagsMappingRule findMapping(String testValue) {
        for(TagsMappingRule travelTag : tagsMapping) {
            if(travelTag.getTaxonomyTag().equals(testValue)) {
                return travelTag;
            }
        }
        return null;
    }

    public static TagsMappingRule findLocationMapping(String testValue){
        int destIndex= testValue.indexOf("destinations");
        if (destIndex == -1){
          return  null;
        }
        String coreValue = "locations:" + testValue.substring(destIndex+13, testValue.length());
        return new TagsMappingRule(testValue,coreValue);
    }
}
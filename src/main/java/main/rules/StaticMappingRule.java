package main.rules;

public class StaticMappingRule {
	
	private String testName;
	private String testNode;
	private String goldValue;
	
	public StaticMappingRule(String testName, String testNode, String goldValue) {
		this.testName = testName;
		this.testNode = testNode;
		this.goldValue = goldValue;
	}

	public String getTestName() {
		return this.testName;
	}
	
	public String getTestNode() {
		return this.testNode;
	}	

	public String getGoldValue() {
		return this.goldValue;
	}
}

package main.rules;

public class TagsMappingRule {

    private String taxonomyTag;
    private String coreTag;
    private String tagGroup;

    public TagsMappingRule(String taxonomyTag, String coreTag, String tagGroup) {
        this.taxonomyTag = taxonomyTag;
        this.coreTag = coreTag;
        this.tagGroup = tagGroup;
    }

    public TagsMappingRule(String taxonomyTag, String coreTag) {
        this.taxonomyTag = taxonomyTag;
        this.coreTag = coreTag;
    }

    public String getTaxonomyTag() {
        return this.taxonomyTag;
    }

    public String getCoreTag() {
        return this.coreTag;
    }

    public String getTagGroup() {
        return this.tagGroup;
    }

}

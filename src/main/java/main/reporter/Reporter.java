package main.reporter;

import java.io.FileWriter;
import java.io.IOException;

import main.core.Config;
import main.core.Failure;

public final class Reporter {
	
	private Reporter() {
	}
	
	public static void log(Failure failure) {
		log(
				failure.getSymptom(),
				failure.getPage(),
				failure.getComponent(),
				failure.getGoldPath(),
				failure.getGoldProperty(),
				failure.getGoldValue(),
				failure.getTestPath(),
				failure.getTestProperty(),
				failure.getTestValue());
	}
	
	public static void log(
			String symptom,
			String page,
			String component,
			String goldPath,
			String goldProperty,
			String goldValue,
			String testPath,
			String testProperty,
			String testValue) {
		
		System.err.println(String.format(
				"%s: %s[%s]{%s} <==> %s[%s]{%s}",
				symptom,
				goldPath,
				goldProperty,
				goldValue,
				testPath,
				testProperty,
				testValue));
		
		FileWriter writer;
		try {
			writer = new FileWriter(Config.getOutputFilePath(), true);
			writer.write(String.format(
					"%s,%s,%s,%s,%s,%s,%s,%s,%s\n",
					symptom,
					page,
					component,
					goldPath,
					goldProperty,
					(goldValue.length() < 1000 ? goldValue : goldValue.substring(0, 1000) + "{1000+}")
						.replace(",", "{comma}")
						.replace(System.getProperty("line.separator"), "{br}")
						.replace("\r\n", "{br}")
						.replace("\n\r", "{br}")
						.replace("\r", "{br}")
						.replace("\n", "{br}"),
					testPath,
					testProperty,
					(testValue.length() < 1000 ? testValue : testValue.substring(0, 1000) + "{1000+}")
						.replace(",", "{comma}")
						.replace(System.getProperty("line.separator"), "{br}")
						.replace("\r\n", "{br}")
						.replace("\n\r", "{br}")
						.replace("\r", "{br}")
						.replace("\n", "{br}")));
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}

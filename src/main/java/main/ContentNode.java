package main;

import main.core.*;
import main.reporter.Reporter;
import main.rules.MappingRule;
import main.rules.StaticMappingRule;
import main.rules.TagsMappingRuleSingleton;
import main.rules.TagsMappingRule;

import javax.jcr.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public abstract class ContentNode {

	protected Node node;
	protected List<MappingRule> map = new ArrayList<MappingRule>();
	protected List<StaticMappingRule> staticMap = new ArrayList<StaticMappingRule>();

	protected void addGlobalProperties() {
		fillMap("src/main/resources/pages/global.txt");
		fillStaticMap("src/main/resources/pages/global-static.txt");
	}

	protected void clearGlobalProperties(){
		map.clear();
		staticMap.clear();
	}

	protected void fillMap(String path) {
		FileReader fileReader;
		try {
			fileReader = new FileReader(path);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;

			while ((line = bufferedReader.readLine()) != null) {
				if (!line.startsWith("#")) {
					String[] tmp = line.split(",", -1);
					map.add(new MappingRule(tmp[0], tmp[1], tmp[2], tmp[3]));
				}
			}
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void fillStaticMap(String path) {
		FileReader fileReader;
		try {
			fileReader = new FileReader(path);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;

			while ((line = bufferedReader.readLine()) != null) {
				if (!line.startsWith("#")) {
					String[] tmp = line.split(",", -1);
					staticMap.add(new StaticMappingRule(tmp[0], tmp[1], tmp[2]));
				}
			}
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void setCurrentTestPath() {
		try {
			String path = node.getPath();
			String subPath = "";
			String result = "";

			if (path.contains("/contestants/")) {
				subPath = path.replace(path.substring(0, path.indexOf("/contestants/") + 13), "");

				String[] nameParts = subPath.split("/")[0].split("-");
				String subFolder = nameParts[nameParts.length - 1].substring(0, 1) + "/";

				result = Config.getTestPath() + "profiles/contestants/" + subFolder + subPath;

			} else if (path.contains("/judges/")) {
				subPath = path.replace(path.substring(0, path.indexOf("/judges/") + 8), "");

				String[] nameParts = subPath.split("/")[0].split("-");
				String subFolder = nameParts[nameParts.length - 1].substring(0, 1) + "/";

				result = Config.getTestPath() + "profiles/talent/" + subFolder + subPath;

			} else if (path.startsWith(Config.getGoldPath() + "chefs/") ||
					path.startsWith(Config.getGoldPath() + "hosts/")) {

				subPath = path
						.replace(Config.getGoldPath() + "chefs/", "")
						.replace(Config.getGoldPath() + "hosts/", "");

				String subFolder = "";

				if (!path.contains("/chefs-a-z/") &&
						!path.contains("chefs/articles/") &&
						!path.contains("chefs/photos/") &&
						!path.contains("hosts/articles/") &&
						!path.contains("hosts/photos/") &&
						!path.contains("/chefs-at-home/") &&
						!path.contains("/gc/")) {

					String[] nameParts = subPath.split("/")[0].split("-");
					subFolder = nameParts[nameParts.length - 1].substring(0, 1) + "/";
				}

				result = Config.getTestPath() + "profiles/talent/" + subFolder + subPath;

			} else if (path.contains("/host/")) {
				result = path
						.replace(Config.getGoldPath(), Config.getTestPath())
						.replace("/host/", "/talent/");

			} else if(path.contains("/cook/") && path.contains("/photo-galleries/")){
				result = path
						.replace(Config.getGoldPath(), Config.getTestPath())
						.replace("/photo-galleries/", "/photos/");

			} else if(path.contains("/tccom/") && path.contains("/editor/")){
				result = path
						.replace(Config.getGoldPath(), Config.getTestPath())
						.replace("/editor/", "/editorial/");
			} else if(path.contains("/tccom/") && path.contains("/videoPlaylist/")){
				result = path
						.replace(Config.getGoldPath(), Config.getTestPath())
						.replace("/videoPlaylist/", "/videoPlaylistPromo/");
			} else if(path.contains("/sni-asset/travelchannel/videos/")){
				result = path
						.replace("/travelchannel/", "/travel/");
			} else {
				result = path
						.replace(Config.getGoldPath(), Config.getTestPath())
						.replace("/shows1/", "/shows/");
			}

			Global.setCurrentTestPath(result + "/");
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

	public void validate() {
		validateSimple();
		validateStatic();
		validateVanity();
		validateTags();
		validateComplex();
		validateSponsorship();
	}

	private void validateTags() {
		try {

			try {
				node.getProperty("primaryTaxonomy").getString();
			} catch (PathNotFoundException e) {
				return;
			}

			checkPrimaryTags("primaryTaxonomy", false);
			checkPrimaryTags("primaryInterest", false);
			checkPrimaryTags("primaryDestination", true);

			getSecondaryPropertyValues("secondaryTaxonomy", false,"additionalTagGroup");
			getSecondaryPropertyValues("secondaryDestinations", true, "secondaryLocation");
			getSecondaryPropertyValues("secondaryInterest", false, "additionalTagGroup");

		} catch (RepositoryException e2) {
			e2.printStackTrace();
			Reporter.log(
					"TAG MAJOR EXCEPTION",
					Global.getPageType(),
					"TAGS",
					"",
					"",
					"",
					Global.getCurrentTestPath(),
					"", "");
			return;
		}
	}

	private void getSecondaryPropertyValues(String testPropertyName, boolean isLocation, String additionalGroupName) {
		Property secondaryTaxonomy = null;
		List<String> secondaryTagValue = new ArrayList<String>();

		try {

			secondaryTaxonomy = node.getProperty(testPropertyName);

			if (secondaryTaxonomy.isMultiple()) {
				for (Value tagValue : secondaryTaxonomy.getValues()) {
					secondaryTagValue.add(tagValue.getString());
				}
			} else {
				secondaryTagValue.add(secondaryTaxonomy.getValue().getString());
			}

			if (isLocation){
				validateSecondaryTags(testPropertyName, secondaryTagValue, isLocation,"locations",additionalGroupName);
			} else{
				validateSecondaryTags(testPropertyName, secondaryTagValue, false,"tags",additionalGroupName);
			}

		} catch (PathNotFoundException e) {
			return;

		} catch (RepositoryException e2) {
			e2.printStackTrace();
			Reporter.log(
					"TAG MAJOR EXCEPTION FOR SECONDARY",
					Global.getPageType(),
					"TAGS",
					"",
					"",
					"",
					Global.getCurrentTestPath() + additionalGroupName,
					"", "");
			return;
		}
	}

	private void checkPrimaryTags(String testPropertyName, boolean isLocation) {
		try {

			String testValue = node.getProperty(testPropertyName).getString();
			if(isLocation){
				validatePrimaryTags(testValue, "primaryLocation", testPropertyName, "locations", isLocation);
			} else{
				validatePrimaryTags(testValue, "primaryTagGroup", testPropertyName, "tags", false);
			}

		} catch (PathNotFoundException e) {

		} catch (RepositoryException e2) {

		}
	}

	private void validateSecondaryTags(String testPropertyName, List<String> tagValues, boolean isLocation, String relativePath, String additionalTagName) {
		try {
			List<String> testTagValue = new ArrayList<String>();
			for (String tagValue : tagValues) {
				TagsMappingRule testValue = isLocation ? TagsMappingRuleSingleton.getInstance().findLocationMapping(tagValue)
						: TagsMappingRuleSingleton.getInstance().findMapping(tagValue);
				if (testValue == null) {
					Reporter.log(
							"TAG MAPPING IS MISSING",
							Global.getPageType(),
							"TAGS",
							node.getPath(),
							testPropertyName,
							tagValue,
							Global.getCurrentTestPath() + relativePath,
							additionalTagName, "");
					continue;
				}
				testTagValue.add(testValue.getCoreTag());
			}

			for (String tagValue : testTagValue) {
				boolean valueTagExist = false;

				NodeIterator nodeIterator = Sessions.getTestSession()
						.getNode(Global.getCurrentTestPath() + relativePath).getNodes();

				while (nodeIterator.hasNext()) {
					Node node = nodeIterator.nextNode();
					String actualTestValue = "";

					try {
						Property propertyValue =  node.getProperty(additionalTagName);
						if(propertyValue.isMultiple()){
							for (Value value : propertyValue.getValues()){
								if(value != null){
									actualTestValue += value.getString() + " ";
								}
							}
						} else{
							actualTestValue = propertyValue.getValue().getString();
						}

					} catch (PathNotFoundException eNode) {
						Reporter.log(
								"TAG PROPERTY IS MISSING",
								Global.getPageType(),
								"TAGS",
								node.getPath(),
								additionalTagName,
								tagValue,
								Global.getCurrentTestPath() +  relativePath,
								"primaryTagGroup", "");
						return;
					}

					if (actualTestValue.contains(tagValue)) {
						valueTagExist = true;
						break;
					}
				}

				if (!valueTagExist) {
					Reporter.log(
							"TAG PROPERTY VALUE DOESN'T EXIST",
							Global.getPageType(),
							"TAGS",
							node.getPath(),
							testPropertyName,
							tagValue,
							Global.getCurrentTestPath() +  relativePath+ "/" + additionalTagName,
							testPropertyName, "");
				}
			}

		} catch (RepositoryException e2) {

		}
	}

	private void validatePrimaryTags(String taxonomy, String testPropertyName, String goldPropertyName, String relativePath, boolean isLocation) {
		String actualTestValue = "";
		relativePath = relativePath.replace("0", "");

		try {
			try {

				Property property = Sessions.getTestSession()
						.getNode(Global.getCurrentTestPath() + relativePath)
						.getProperty(testPropertyName);

				if(property.isMultiple()){
					for (Value value : property.getValues()){
						if(value != null){
							actualTestValue += value.getString() + " ";
						}
					}
				} else{
					actualTestValue = property.getValue().getString();
				}

			} catch (PathNotFoundException eNode) {
				Reporter.log(
						"TAG PROPERTY IS MISSING",
						Global.getPageType(),
						"TAGS",
						node.getPath(),
						goldPropertyName,
						taxonomy,
						Global.getCurrentTestPath() + relativePath,
						testPropertyName, "");
				return;
			}

			TagsMappingRule testValue = isLocation ? TagsMappingRuleSingleton.getInstance().findLocationMapping(taxonomy)
					: TagsMappingRuleSingleton.getInstance().findMapping(taxonomy);

			if (testValue == null || !actualTestValue.contains(testValue.getCoreTag())) {
				Reporter.log(
						"TAG PROPERTY VALUE MISMATCH",
						Global.getPageType(),
						"TAGS",
						node.getPath(),
						goldPropertyName,
						taxonomy,
						Global.getCurrentTestPath() + relativePath,
						testPropertyName, actualTestValue);
				return;
			}
		} catch (RepositoryException e2) {
			e2.printStackTrace();
			Reporter.log(
					"TAG MAJOR EXCEPTION",
					Global.getPageType(),
					"TAGS",
					"",
					goldPropertyName,
					taxonomy,
					Global.getCurrentTestPath() + relativePath,
					testPropertyName, "");
			return;
		}
	}

	protected void validateComplex() {
	}

	protected void validateDynamicStatic(){
		try {
			Property property = node.getProperty("type");
			String value = "";
			if (property.isMultiple()) {
				for (Value tempValue : property.getValues()) {
					if (tempValue != null) {
						value += tempValue.getString() + " ";
					}
				}
			} else {
				value = property.getString();
			}
			if (!value.contains("dynamic")) {
				return;
			}

		} catch (RepositoryException ex) {

		}

		for (StaticMappingRule rule : staticMap) {
			try {
				validationProcedure(
						"",
						"",
						rule.getGoldValue(),
						rule.getTestNode(),
						rule.getTestName());
			} catch (Exception e) {
				try {
					e.printStackTrace();
					Reporter.log(
							"MAJOR EXCEPTION",
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							node.getPath(),
							"",
							"",
							"",
							"",
							"");
				} catch (RepositoryException e1) {
					e1.printStackTrace();
					Reporter.log(
							"MAJOR EXCEPTION",
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							"",
							"",
							"",
							"",
							"",
							"");
				}
			}
		}
	}

	private void validateSponsorship() {

		try {

			String sponsorshipGoldPath = null;
			String goldJcrTitle = null;
			String testSponsorshipValue = null;

			try {
				sponsorshipGoldPath = node.getProperty("sni:sponsorship").getString();
			} catch (PathNotFoundException e) {
				return;
			}

			try {
				goldJcrTitle = Sessions.getGoldSession()
						.getNode(sponsorshipGoldPath)
						.getNode("jcr:content")
						.getProperty("jcr:title")
						.getString();
			} catch (PathNotFoundException e) {
				Reporter.log(
						"SPONSORSHIP ORIGINAL JCR:TITLE IS MISSING",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						node.getPath(),
						"sni:sponsorship",
						"",
						"",
						"",
						"");
				return;
			}

			String testPath = Global.getCurrentTestPath();

			if (testPath == null) {
				Reporter.log(
						"VIDEO PATH CONVERSION ERROR",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						node.getPath(),
						"sni:sponsorship",
						goldJcrTitle,
						"",
						"sni:sponsorship",
						"");
				return;
			}

			try {
				testSponsorshipValue = Sessions.getTestSession()
						.getNode(testPath)
						.getProperty("sni:sponsorship")
						.getString();
			} catch (PathNotFoundException e) {
				Reporter.log(
						"SPONSORSHIP PROPERTY IS MISSING",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						node.getPath(),
						"sni:sponsorship",
						goldJcrTitle,
						testPath,
						"sni:sponsorship",
						"");
				return;
			}

			if (!goldJcrTitle.equals(testSponsorshipValue)) {
				Reporter.log(
						"SPONSORSHIP PROPERTY VALUE MISMATCH",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						node.getPath(),
						"sni:sponsorship",
						goldJcrTitle,
						testPath,
						"sni:sponsorship",
						testSponsorshipValue);
			}

		} catch (RepositoryException e) {
			try {
				e.printStackTrace();
				Reporter.log(
						"SPONSORSHIP MAJOR EXCEPTION",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						node.getPath(),
						"sni:sponsorship",
						"",
						"",
						"",
						"");
				return;
			} catch (RepositoryException e2) {
				e2.printStackTrace();
				Reporter.log(
						"SPONSORSHIP MAJOR EXCEPTION",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						"",
						"sni:sponsorship",
						"",
						"",
						"",
						"");
				return;
			}
		}
	}

	private void validateVanity() {

		List<Value> goldVanities = new ArrayList<Value>();
		List<String> testVanities = new ArrayList<String>();

		try {
			if (node.getProperty("sling:vanityPath").isMultiple()) {
				goldVanities.addAll(Arrays.asList(node.getProperty("sling:vanityPath").getValues()));
			} else {
				goldVanities.add(node.getProperty("sling:vanityPath").getValue());
			}
		} catch (PathNotFoundException eGold) {
			return;
		} catch (RepositoryException e) {
			try {
				e.printStackTrace();
				Reporter.log(
						"VANITY MAJOR EXCEPTION",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						node.getPath(),
						"sling:vanityPath",
						"",
						"",
						"",
						"");
				return;
			} catch (RepositoryException e3) {
				e3.printStackTrace();
				Reporter.log(
						"VANITY MAJOR EXCEPTION",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						"",
						"sling:vanityPath",
						"",
						"",
						"",
						"");
				return;
			}
		}

		String testPath = Global.getCurrentTestPath();

		if (testPath == null) {
			try {
				Reporter.log(
						"VIDEO PATH CONVERSION ERROR",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						node.getPath(),
						"sling:vanityPath",
						"",
						"",
						"",
						"");
			} catch (RepositoryException e) {
				e.printStackTrace();
				Reporter.log(
						"VIDEO PATH CONVERSION ERROR",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						"",
						"sling:vanityPath",
						"",
						"",
						"",
						"");
			}
			return;
		}

		testPath = testPath
				.replace("/jcr:content/", "")
				.replaceFirst("/content/food-com/", "/content/siteredirects/www.foodnetwork.com/")
				.replaceFirst("/content/cook-com/", "/content/siteredirects/www.cookingchanneltv.com/");

		NodeIterator vanityNodes = null;

		try {
			vanityNodes = Sessions.getTestSession().getNode(testPath).getNodes();

			while (vanityNodes.hasNext()) {
				Node cursor = vanityNodes.nextNode();
				try {
					testVanities.add(
							cursor
									.getNode("jcr:content")
									.getProperty("redirectOriginal")
									.getValue()
									.getString());
				} catch (PathNotFoundException e) {
					continue;
				}
			}
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		for (Value goldVanity : goldVanities) {
			try {

				String strippedGoldVanity = goldVanity
						.getString()
						.replaceFirst("http://www.fooodnetwork.com:80", "")
						.replaceFirst("http://www.foodnetwork.com:80", "")
						.replaceFirst("http://www.foodnetwork.com:40", "")
						.replaceFirst("http://www.foodnetwork.com:20", "")
						.replaceFirst("http://www.foodnetwork.com", "")
						.replaceFirst("http:/www.foodnetwork.com:80", "")
						.replaceFirst("http://foodnetwork.com:80", "")
						.replaceFirst("http:/foodnetwork.com:80", "")
						.replaceFirst("http://foodnetwork.com", "")
						.replaceFirst("www.foodnetwork.com:80", "")
						.replaceFirst("foodnetwork.com:80", "")
						.replaceFirst("http://www.cookingchanneltv.com:80", "")
						.replaceFirst("http://www.cookingchanneltv.com:40", "")
						.replaceFirst("http://www.cookingchanneltv.com:20", "")
						.replaceFirst("http://www.cookingchanneltv.com", "")
						.replaceFirst("http://cookingchanneltv.com:80", "")
						.replaceFirst("http://cookingchanneltv.com", "")
						.replaceFirst("www.cookingchanneltv.com:80", "")
						.replaceFirst("cookingchanneltv.com:80", "");

				if (!testVanities.contains(strippedGoldVanity.replace(".html", ""))) {
					if (testVanities.contains(strippedGoldVanity)) {
						Reporter.log(new Failure(
								"VANITY MISMATCH (.html)",
								Global.getPageType(),
								this.getClass().getSimpleName().equals(Global.getPageType()) ?
										"" : this.getClass().getSimpleName(),
								node.getPath(),
								"sling:vanityPath",
								goldVanity.getString(),
								testPath,
								"redirectOriginal",
								""));
					} else {
						Reporter.log(new Failure(
								"VANITY IS MISSING",
								Global.getPageType(),
								this.getClass().getSimpleName().equals(Global.getPageType()) ?
										"" : this.getClass().getSimpleName(),
								node.getPath(),
								"sling:vanityPath",
								goldVanity.getString(),
								testPath,
								"redirectOriginal",
								""));
					}
				}
			} catch (RepositoryException e) {
				e.printStackTrace();
				try {
					e.printStackTrace();
					Reporter.log(
							"VANITY MAJOR EXCEPTION",
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							node.getPath(),
							"",
							"",
							"",
							"",
							"");
					return;
				} catch (RepositoryException e3) {
					e3.printStackTrace();
					Reporter.log(
							"VANITY MAJOR EXCEPTION",
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							"",
							"",
							"",
							"",
							"",
							"");
					return;
				}
			}
		}
	}

	protected void validateSimple() {


		for (MappingRule rule : map) {

			try {

				Node goldNode;
				Property goldProperty;
				try {
					goldNode = rule.getGoldNode().isEmpty() ? node : node.getNode(rule.getGoldNode());
					goldProperty = goldNode.getProperty(rule.getGoldName());
				} catch (PathNotFoundException eGold) {
					continue;
				}

				String goldValue;
				try {
					goldValue = goldProperty.getValue().getString();
				} catch (ValueFormatException eFormat) {
					if (goldProperty.getValues().length > 0) {
						goldValue = goldProperty.getValues()[0].getString();
					} else {
						goldValue = "";
					}
				}

				validationProcedure(
						goldNode.getPath(),
						rule.getGoldName(),
						PathConverter.getConvertedPath(goldValue),
						rule.getTestNode(),
						rule.getTestName());

			} catch (Exception e) {
				try {
					e.printStackTrace();
					Reporter.log(
							"MAJOR EXCEPTION",
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							node.getPath(),
							"",
							"",
							"",
							"",
							"");
				} catch (RepositoryException e1) {
					e1.printStackTrace();
					Reporter.log(
							"MAJOR EXCEPTION",
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							"",
							"",
							"",
							"",
							"",
							"");
				}
			}
		}
	}

	protected void validateStatic() {

		for (StaticMappingRule rule : staticMap) {

			try {
				validationProcedure(
						"",
						"",
						rule.getGoldValue(),
						rule.getTestNode(),
						rule.getTestName());
			} catch (Exception e) {
				try {
					e.printStackTrace();
					Reporter.log(
							"MAJOR EXCEPTION",
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							node.getPath(),
							"",
							"",
							"",
							"",
							"");
				} catch (RepositoryException e1) {
					e1.printStackTrace();
					Reporter.log(
							"MAJOR EXCEPTION",
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							"",
							"",
							"",
							"",
							"",
							"");
				}
			}
		}
	}

	protected void validationProcedure(String goldPath, String goldName, String goldValue,
									   String relativeTestPath, String testName) throws RepositoryException {

		Failure failure = null;

		String testPath = Global.getCurrentTestPath();

		if (testPath == null || goldValue == null) {
			Reporter.log(
					"VIDEO PATH CONVERSION ERROR",
					Global.getPageType(),
					this.getClass().getSimpleName().equals(Global.getPageType()) ?
							"" : this.getClass().getSimpleName(),
					goldPath,
					goldName,
					goldValue,
					testPath,
					testName,
					"");
			return;
		}

		testPath += relativeTestPath;

		if (testPath.endsWith("/")) {
			testPath = testPath.substring(0, testPath.length() - 1);
		}
		String nodePattern = testPath.substring(testPath.lastIndexOf("/") + 1) + "*";
		testPath = testPath.substring(0, testPath.lastIndexOf("/"));


		System.out.println("-node pattern- " + nodePattern);
		System.out.println("-test path- " + testPath);


		if ("Episode".equals(Global.getPageType())) {
			testPath = testPath.replace("articleBody", "contentWell");
		}

		System.out.println(String.format(
				"validating property %s[%s] <==> %s[%s]",
				goldPath,
				goldName,
				testPath,
				testName));

		try {
			Sessions.getTestSession().getNode(Global.getCurrentTestPath());
		} catch (PathNotFoundException eNode) {
			failure = new Failure(
					"PAGE IS MISSING",
					Global.getPageType(),
					this.getClass().getSimpleName().equals(Global.getPageType()) ?
							"" : this.getClass().getSimpleName(),
					goldPath,
					goldName,
					goldValue,
					testPath,
					"",
					"");
			Reporter.log(failure);
			return;
		}

		NodeIterator nodes = null;

		try {
			nodes = Sessions.getTestSession().getNode(testPath).getNodes(nodePattern);
		} catch (PathNotFoundException eNode) {
			failure = new Failure(
					"PARENT IS MISSING",
					Global.getPageType(),
					this.getClass().getSimpleName().equals(Global.getPageType()) ?
							"" : this.getClass().getSimpleName(),
					goldPath,
					goldName,
					goldValue,
					testPath,
					"",
					"");
			Reporter.log(failure);
			return;
		}

		if (!nodes.hasNext()) {
			failure = new Failure(
					"NODE IS MISSING",
					Global.getPageType(),
					this.getClass().getSimpleName().equals(Global.getPageType()) ?
							"" : this.getClass().getSimpleName(),
					goldPath,
					goldName,
					goldValue,
					testPath,
					"",
					"");
		} else {
			while (nodes.hasNext()) {
				failure = getValidationResult(
						goldPath,
						goldName,
						goldValue,
						nodes.nextNode(),
						testName);
				if (failure == null) {
					return;
				}
			}
			if (nodes.getSize() > 1) {
				failure = new Failure(
						"PROPERTY IS MISSING (multiple nodes)",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						goldPath,
						goldName,
						goldValue,
						testPath,
						testName,
						"");
			} else if (nodes.getSize() < 1) {
				failure = new Failure(
						"MULTIPLE NODES - SIZING ERROR",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						goldPath,
						goldName,
						goldValue,
						testPath,
						testName,
						"");
			}
		}

		Reporter.log(failure);

	}

	protected Failure getValidationResult(String goldPath, String goldName, String goldValue,
										  Node testNode, String testName) throws RepositoryException {

		if(goldValue.contains("/content/travel-com/") || goldValue.contains("/content/tccom/")){
			goldValue = goldValue.replace("/editor/", "/editorial/")
					             .replace("/host/", "/talent/");
		}
		String testPath = testNode.getPath();

		String testValue = null;
		try {
			testValue = testNode.getProperty(testName).getString();
		} catch (ValueFormatException eFormat) {
			if (testNode.getProperty(testName).getValues().length > 0) {
				testValue = testNode.getProperty(testName).getValues()[0].getString();
			} else {
				testValue = "";
			}
		} catch (PathNotFoundException eProperty) {
			return new Failure(
					"PROPERTY IS MISSING",
					Global.getPageType(),
					this.getClass().getSimpleName().equals(Global.getPageType()) ?
							"" : this.getClass().getSimpleName(),
					goldPath,
					goldName,
					goldValue,
					testPath,
					testName,
					"");
		}

		if (!goldValue.replace(Config.getGoldPath(), Config.getTestPath())
				.equalsIgnoreCase(testValue)) {
			return new Failure(
					"PROPERTY VALUE MISMATCH",
					Global.getPageType(),
					this.getClass().getSimpleName().equals(Global.getPageType()) ?
							"" : this.getClass().getSimpleName(),
					goldPath,
					goldName,
					goldValue,
					testPath,
					testName,
					testValue);
		}

		return null;
	}

	protected Failure getValidationResult(String goldPath, String goldName, String goldValue,
										  Node testNode, String testName, String expectedResult) throws RepositoryException {

		String testPath = Global.getCurrentTestPath();
		String testValue = null;

		try {
			if (testNode.getProperty(testName).isMultiple()) {
				testValue = testNode.getProperty(testName).getValues()[0].getString();
			} else {
				testValue = testNode.getProperty(testName).getString();
			}
		} catch (ValueFormatException eFormat) {
			if (testNode.getProperty(testName).getValues().length > 0) {
				testValue = testNode.getProperty(testName).getValues()[0].getString();
			} else {
				testValue = "";
			}
		} catch (PathNotFoundException eProperty) {
			return new Failure(
					"PROPERTY IS MISSING FOR " + goldName,
					Global.getPageType(),
					this.getClass().getSimpleName().equals(Global.getPageType()) ?
							"" : this.getClass().getSimpleName(),
					goldPath,
					goldName,
					goldValue,
					testPath,
					testName,
					"");
		}

		if (!expectedResult.equalsIgnoreCase(testValue)) {
			return new Failure(
					"PROPERTY VALUE MISMATCH FOR " + goldName,
					Global.getPageType(),
					this.getClass().getSimpleName().equals(Global.getPageType()) ?
							"" : this.getClass().getSimpleName(),
					goldPath,
					goldName,
					goldValue,
					testPath,
					testName,
					testValue);
		}

		return null;
	}

	protected void validateAssetLinks(){
		try{
			if(!node.hasProperty("sni:assetLink")){
				return;
			}
			Property assetLinkProperty = node.getProperty("sni:assetLink");
			ArrayList<String> assetLink = getAssetLinkValue(assetLinkProperty);
			if(assetLink == null || assetLink.size() == 0){
				return;
			}

			if(!Sessions.getTestSession().getNode(getTestingNodePath()).hasProperty("sni:assetLink")){
				Reporter.log(
						"MISSING PROPERTY sni:assetLink",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						node.getPath(),
						"sni:assetLink",
						"",
						getTestingNodePath(),
						"sni:assetLink",
						"DOESN'T EXIST");
				return;
			}

			Property assetLinkTestProperty= Sessions.getTestSession().getNode(getTestingNodePath()).getProperty("sni:assetLink");
			ArrayList<String> assetLinkTest = getAssetLinkValue(assetLinkTestProperty);
			if(assetLinkTest == null || assetLink.size()== 0){
				Reporter.log(
						"MISSING VALUE sni:assetLink",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						node.getPath(),
						"sni:assetLink",
						"",
						getTestingNodePath(),
						"sni:assetLink",
						"EMPTY OR DOESN'T EXIST");
				return;
			}
			for(String asset : assetLink){
				if(!assetLinkTest.contains(asset.replace("/trav/","/travel/"))){
					Reporter.log(
							"VALUE MISMATCH sni:assetLink",
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							node.getPath(),
							"sni:assetLink",
							asset,
							node.getPath().replace(Config.getGoldHost(), Config.getTestHost()),
							"sni:assetLink",
							getStringFromList(assetLinkTest));
				}
				if(!Sessions.getTestSession().nodeExists(asset.replace("/trav/","/travel/"))){
					Reporter.log(
							"VALUE WASN'T CREATED sni:assetLink",
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							 node.getPath(),
							"sni:assetLink",
							"",
							node.getPath().replace(Config.getGoldHost(), Config.getTestHost()),
							"sni:assetLink",
							asset.replace("/trav/","/travel/"));
				}
			}


		} catch(Exception ex){
			Reporter.log(
					"MAJOR EXCEPTION sni:assetLink",
					Global.getPageType(),
					this.getClass().getSimpleName().equals(Global.getPageType()) ?
							"" : this.getClass().getSimpleName(),
					"",
					"sni:assetLink",
					"",
					"",
					"sni:assetLink",
					"");
		}
	}

	protected void verifyLandingPages(String subNode, String nodeName) {
		try {

			if (!node.getPath().contains("/destinations/") && !node.getPath().contains("/interests/")) {
				return;
			}

			String testPath = Global.getCurrentTestPath();
			if (!Sessions.getTestSession().nodeExists(testPath + "listType/" + subNode)) {
				Reporter.log(
						"MISSING NODE listType/" + subNode,
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						node.getPath(),
						"",
						"",
						testPath,
						"",
						"");
				return;
			}

			if (testPath.contains("/destinations/")) {
				if (!Sessions.getTestSession().getNode(testPath + "listType/" + subNode).getProperty("query").getValue().getString().contains("selectManually")) {
					Reporter.log(
							"INVALID VALUE listType/" + subNode,
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							node.getPath(),
							"query",
							"selectManually",
							testPath,
							"query",
							"");
				}

				Node tempNode = Sessions.getTestSession().getNode(testPath + "listType/" + subNode + "/locations");
				if(!tempNode.getProperty("jcr:primaryType").getValue().getString().contains("nt:unstructured")){
					Reporter.log(
							"INVALID VALUE listType/" + subNode,
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							node.getPath(),
							"nt:unstructured",
							"jcr:primaryType",
							testPath,
							"jcr:primaryType",
							"");
				}

				if (!Sessions.getTestSession().nodeExists(testPath.replace(nodeName, "/") + "locations")) {
					return;
				}
				Node locationNode = Sessions.getTestSession().getNode(testPath.replace(nodeName, "/") + "locations");
				if (!locationNode.hasProperty("primaryLocation")) {
					return;
				}

				if(!locationNode.getProperty("primaryLocation").getValue().getString().contains(tempNode.getProperty("primaryLocation").getValue().getString())){
					Reporter.log(
							"INVALID VALUE listType/" + subNode,
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							node.getPath(),
							"primaryLocation",
							"",
							testPath,
							"primaryLocation",
							"");
			}


			} else if (testPath.contains("/interests/")) {

				Node tempNode = Sessions.getTestSession().getNode(testPath + "listType/" + subNode);

				if (!tempNode.getProperty("query").getValue().getString().contains("selectManually")) {
					Reporter.log(
							"INVALID VALUE listType/" + subNode,
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							node.getPath(),
							"query",
							"selectManually",
							testPath,
							"query",
							"");
				}
				if (!Sessions.getTestSession().nodeExists(testPath.replace(nodeName, "/") + "tags")) {
					return;
				}

				Node tagNode = Sessions.getTestSession().getNode(testPath.replace(nodeName, "/") + "tags");
				if (!tagNode.hasProperty("primaryTagGroup")) {
					return;
				}
				String tagValue = null;
				String tagValueTemp = null;
				if(tagNode.getProperty("primaryTagGroup").isMultiple()) {
					tagValue = getStringFromList(getAssetLinkValue(tagNode.getProperty("primaryTagGroup")));
				} else{
					tagValue = tagNode.getProperty("primaryTagGroup").getValue().getString();
				}

				if(tempNode.getProperty("tags").isMultiple()) {
					tagValueTemp = getStringFromList(getAssetLinkValue(tempNode.getProperty("tags")));
				} else{
					tagValueTemp = tempNode.getProperty("tags").getValue().getString();
				}

				if (!tagValue.contains(tagValueTemp))
				{
					Reporter.log(
							"INVALID VALUE listType/" + subNode,
							Global.getPageType(),
							this.getClass().getSimpleName().equals(Global.getPageType()) ?
									"" : this.getClass().getSimpleName(),
							node.getPath(),
							"tags",
							tagValue,
							testPath,
							"tags",
							"");
				}
			}
		} catch(Exception ex){
			Reporter.log(
					"EXCEPTION istType/" + subNode,
					Global.getPageType(),
					this.getClass().getSimpleName().equals(Global.getPageType()) ?
							"" : this.getClass().getSimpleName(),
					"",
					"tags",
					"",
					"",
					"tags",
					"");
		}
	}

	private  ArrayList<String> getAssetLinkValue(Property assetLinkProperty){
		ArrayList<String> assetLink = new ArrayList<String>();
		try {
			if (assetLinkProperty.isMultiple()) {
				for (Value value : assetLinkProperty.getValues()) {
					if (value != null) {
						assetLink.add(value.getString());
					}
				}
			} else {
				assetLink.add(assetLinkProperty.getValue().getString());
			}
		} catch(Exception ex){}

		return assetLink;
	}

	private String getTestingNodePath(){
		String testNodePath= null;
		try{
			testNodePath = node.getPath().replace(Config.getGoldPath(), Config.getTestPath());

		} catch(Exception ex){}

		return testNodePath;
	}

	private String getStringFromList(ArrayList<String> input){
		StringBuilder sb = new StringBuilder();
		for(String str : input){
			sb.append(str).append(";");
		}
		return sb.toString();
	}
}

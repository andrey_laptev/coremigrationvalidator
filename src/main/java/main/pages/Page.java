package main.pages;

import javax.jcr.Node;
import main.core.Global;
import main.ContentNode;

public class Page extends ContentNode {
	
	protected Page(Node node, String simpleFilePath, boolean global) {
		this.node = node;
		setCurrentTestPath();
		fillMap(simpleFilePath);
		if (global) {
			addGlobalProperties();
		}
		Global.setPageType(this.getClass().getSimpleName());
	}

	protected Page(Node node, String simpleFilePath) {
		this(node, simpleFilePath, true);
	}
	
	protected Page(Node node, String simpleFilePath, String staticFilePath, boolean global) {
		this (node, simpleFilePath, global);
		fillStaticMap(staticFilePath);
	}
	
	protected Page(Node node, String simpleFilePath, String staticFilePath) {
		this(node, simpleFilePath, staticFilePath, true);
	}
}

package main.pages.fn;

import javax.jcr.Node;
import main.pages.Page;
import main.Validatable;

public class Show extends Page implements Validatable {
	
	public Show(Node node) {
		super(node, "src/main/resources/pages/fn/show.txt",
				"src/main/resources/pages/fn/show-static.txt");
	}

}

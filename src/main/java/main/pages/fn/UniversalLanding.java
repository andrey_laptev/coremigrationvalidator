package main.pages.fn;

import javax.jcr.Node;
import main.pages.Page;
import main.Validatable;

public class UniversalLanding extends Page implements Validatable {
	
	public UniversalLanding(Node node) {
		super(node, "src/main/resources/pages/fn/universal-landing.txt",
				"src/main/resources/pages/fn/universal-landing-static.txt");
	}

}

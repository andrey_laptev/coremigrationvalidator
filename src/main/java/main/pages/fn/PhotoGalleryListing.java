package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class PhotoGalleryListing extends Page implements Validatable {
	
	public PhotoGalleryListing(Node node) {
		super(node, "src/main/resources/pages/fn/photo-gallery-listing.txt",
				"src/main/resources/pages/fn/photo-gallery-listing-static.txt");
	}

}

package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class Bio extends Page implements Validatable {
	
	public Bio(Node node) {
		super(node, "src/main/resources/pages/fn/bio.txt",
				"src/main/resources/pages/fn/bio-static.txt");
	}

}

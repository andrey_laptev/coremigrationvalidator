package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class VideoChannel extends Page implements Validatable {
	
	public VideoChannel(Node node) {
		super(node, "src/main/resources/pages/fn/video-channel.txt",
				"src/main/resources/pages/fn/video-channel-static.txt");
	}

}

package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class Topic extends Page implements Validatable {
	
	public Topic(Node node) {
		super(node, "src/main/resources/pages/fn/topic.txt",
				"src/main/resources/pages/fn/topic-static.txt");
	}

}

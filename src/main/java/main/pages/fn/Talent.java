package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class Talent extends Page implements Validatable {
	
	public Talent(Node node) {
		super(node, "src/main/resources/pages/fn/talent.txt",
				"src/main/resources/pages/fn/talent-static.txt");
	}

}

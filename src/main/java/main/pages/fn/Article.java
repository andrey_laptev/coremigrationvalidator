package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class Article extends Page implements Validatable {
	
	public Article(Node node) {
		super(node, "src/main/resources/pages/fn/article.txt",
				"src/main/resources/pages/fn/article-static.txt");
	}

}

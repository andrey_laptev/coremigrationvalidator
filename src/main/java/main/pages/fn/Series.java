package main.pages.fn;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import main.pages.Page;
import main.Validatable;
import main.core.Global;
import main.core.PathConverter;

public class Series extends Page implements Validatable {
	
	public Series(Node node) {
		super(node, "src/main/resources/pages/fn/series.txt",
				"src/main/resources/pages/fn/series-static.txt");
	}
	
	@Override
	protected void setCurrentTestPath() {
		try {
			Global.setCurrentTestPath(
					PathConverter.getConvertedSeriesPath(node.getPath()));
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}
	
}

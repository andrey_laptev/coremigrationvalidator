package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class Menu extends Page implements Validatable {
	
	public Menu(Node node) {
		super(node, "src/main/resources/pages/fn/menu.txt",
				"src/main/resources/pages/fn/menu-static.txt");
	}

}

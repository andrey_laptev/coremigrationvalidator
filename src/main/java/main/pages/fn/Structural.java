package main.pages.fn;

import javax.jcr.Node;
import main.pages.Page;
import main.Validatable;

public class Structural extends Page implements Validatable {
	
	public Structural(Node node) {
		super(node, "src/main/resources/pages/fn/structural.txt",
				"src/main/resources/pages/fn/structural-static.txt");
	}

}

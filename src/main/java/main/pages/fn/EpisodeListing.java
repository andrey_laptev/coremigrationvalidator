package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class EpisodeListing extends Page implements Validatable {
	
	public EpisodeListing(Node node) {
		super(node, "src/main/resources/pages/fn/episode-listing.txt",
				"src/main/resources/pages/fn/episode-listing-static.txt");
	}

}

package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class VideoPlayer extends Page implements Validatable {
	
	public VideoPlayer(Node node) {
		super(node, "src/main/resources/pages/fn/video-player.txt",
				"src/main/resources/pages/fn/video-player-static.txt");
	}

}

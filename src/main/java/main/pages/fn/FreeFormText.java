package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class FreeFormText extends Page implements Validatable {
	
	public FreeFormText(Node node) {
		super(node, "src/main/resources/pages/fn/free-form-text.txt",
				"src/main/resources/pages/fn/free-form-text-static.txt");
	}

}

package main.pages.fn;

import javax.jcr.Node;
import main.pages.Page;
import main.Validatable;

public class Company extends Page implements Validatable {
	
	public Company(Node node) {
		super(node, "src/main/resources/pages/fn/company.txt",
				"src/main/resources/pages/fn/company-static.txt");
	}

}

package main.pages.fn;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import main.pages.Page;
import main.Validatable;
import main.core.Global;
import main.core.PathConverter;

public class SingleVideo extends Page implements Validatable {
	
	public SingleVideo(Node node) {
		super(node, "src/main/resources/pages/fn/single-video.txt",
				"src/main/resources/pages/fn/single-video-static.txt", false);
	}
	
	@Override
	protected void setCurrentTestPath() {
		try {
			Global.setCurrentTestPath(
					PathConverter.getFNConvertedSingleVideoPath(node.getPath())
					 + "/jcr:content/");
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}
	
}

package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class PhotoGallery extends Page implements Validatable {
	
	public PhotoGallery(Node node) {
		super(node, "src/main/resources/pages/fn/photo-gallery.txt",
				"src/main/resources/pages/fn/photo-gallery-static.txt");
	}

}

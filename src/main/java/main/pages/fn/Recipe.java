package main.pages.fn;

import main.Validatable;
import main.core.Failure;
import main.core.Global;
import main.core.Sessions;
import main.pages.Page;
import main.reporter.Reporter;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import java.util.HashMap;
import java.util.Map;

public class Recipe extends Page implements Validatable {
	
	public Recipe(Node node) {
		super(node, "src/main/resources/pages/fn/recipe.txt",
				"src/main/resources/pages/fn/recipe-static.txt", true);
	}
	
	@Override
	protected void validateComplex() {
		validateVinePairings();
	}
	
	private void validateVinePairings() {
		Map<String, String> pathMap = new HashMap<String, String>();
		pathMap.put("/content/modules/beverages/merlot", "/content/food-com/en/terms/m/merlot");
		pathMap.put("/content/modules/beverages/cabernet-sauvignon", "/content/food-com/en/terms/c/cabernet-sauvignon");
		pathMap.put("/content/modules/beverages/chardonnay", "/content/food-com/en/terms/c/chardonnay");
		pathMap.put("/content/modules/beverages/dark-beer", "/content/food-com/en/terms/d/dark-beer");
		pathMap.put("/content/modules/beverages/lager", "/content/food-com/en/terms/l/lager");
		pathMap.put("/content/modules/beverages/pinot-grigio", "/content/food-com/en/terms/p/pinot-grigio");
		pathMap.put("/content/modules/beverages/pinot-noir", "/content/food-com/en/terms/p/pinot-noir");
		pathMap.put("/content/modules/beverages/riesling", "/content/food-com/en/terms/r/riesling");
		pathMap.put("/content/modules/beverages/sangiovese", "/content/food-com/en/terms/s/sangiovese");
		pathMap.put("/content/modules/beverages/sauvignon-blanc", "/content/food-com/en/terms/s/sauvignon-blanc");
		pathMap.put("/content/modules/beverages/sparkling-wine", "/content/food-com/en/terms/s/sparkling-wine");

		Map<String, String> nameMap = new HashMap<String, String>();
		nameMap.put("Merlot", "/content/food-com/en/terms/m/merlot");
		nameMap.put("Cabernet Sauvignon", "/content/food-com/en/terms/c/cabernet-sauvignon");
		nameMap.put("Chardonnay", "/content/food-com/en/terms/c/chardonnay");
		nameMap.put("Dark Beer", "/content/food-com/en/terms/d/dark-beer");
		nameMap.put("Lager Beer", "/content/food-com/en/terms/l/lager");
		nameMap.put("Pinot Grigio", "/content/food-com/en/terms/p/pinot-grigio");
		nameMap.put("Pinot Noir", "/content/food-com/en/terms/p/pinot-noir");
		nameMap.put("Riesling", "/content/food-com/en/terms/r/riesling");
		nameMap.put("Sangiovese", "/content/food-com/en/terms/s/sangiovese");
		nameMap.put("Sauvignon Blanc", "/content/food-com/en/terms/s/sauvignon-blanc");
		nameMap.put("Sparkling Wine", "/content/food-com/en/terms/s/sparkling-wine");
		
		String goldPairingValue = "";
		try {
			try {
				String goldPairingPath = null;
				System.out.println("-testing vine pairing for node- " + node.getPath());
				if(node.getProperty("sni:pairingLinks").isMultiple()){
					goldPairingPath = node.getProperty("sni:pairingLinks").getValues()[0].getString();
				} else{
					goldPairingPath = node.getProperty("sni:pairingLinks").getString();
				}

				goldPairingValue = pathMap.get(goldPairingPath);
			} catch (PathNotFoundException e1) {
				try {
					String assetPath = node.getProperty("sni:assetLink").getString();
					String goldPairingName =
							Sessions.getGoldSession().getNode(assetPath).getProperty("sni:beveragePairing").getString();
					goldPairingValue = nameMap.get(goldPairingName);
				} catch (PathNotFoundException e2) {
					return;
				}
			}
			
			try {
				Sessions.getTestSession().getNode(Global.getCurrentTestPath());
			} catch (PathNotFoundException eNode) {
				Reporter.log(
						"PAGE IS MISSING",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						node.getPath(),
						"sni:pairingLinks",
						goldPairingValue,
						Global.getCurrentTestPath(),
						"sni:pairingLinks",
						"");
				return;
			}
			
			Failure failure = getValidationResult(
					node.getPath(),
					"sni:pairingLinks",
					goldPairingValue,
					Sessions.getTestSession().getNode(Global.getCurrentTestPath()),
					"sni:pairingLinks",
					goldPairingValue);
			
			if (failure != null) {
				Reporter.log(failure);
			}
			
		} catch (RepositoryException e3) {
			try {
				e3.printStackTrace();
				Reporter.log(
						"MAJOR EXCEPTION BEVERAGE",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						node.getPath(),
						"",
						"",
						"",
						"",
						"");
			} catch (RepositoryException e1) {
				e1.printStackTrace();
				Reporter.log(
						"MAJOR EXCEPTION BEVERAGE",
						Global.getPageType(),
						this.getClass().getSimpleName().equals(Global.getPageType()) ?
								"" : this.getClass().getSimpleName(),
						"",
						"",
						"",
						"",
						"",
						"");
			}
			return;
		}
		
	}

}

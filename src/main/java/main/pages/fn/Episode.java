package main.pages.fn;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import main.pages.Page;
import main.Validatable;
import main.core.Global;
import main.core.PathConverter;

public class Episode extends Page implements Validatable {
	
	public Episode(Node node) {
		super(node, "src/main/resources/pages/fn/episode.txt",
				"src/main/resources/pages/fn/episode-static.txt");
	}
	
	@Override
	protected void setCurrentTestPath() {
		try {
			Global.setCurrentTestPath(
					PathConverter.getConvertedEpisodePath(node.getPath()));
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

}

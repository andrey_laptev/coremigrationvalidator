package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class AssetRecipes extends Page implements Validatable {
	
	public AssetRecipes(Node node) {
		super(node, "src/main/resources/pages/fn/asset-recipes.txt",
				"src/main/resources/pages/fn/asset-recipes-static.txt");
	}

}

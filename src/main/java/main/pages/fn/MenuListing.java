package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class MenuListing extends Page implements Validatable {
	
	public MenuListing(Node node) {
		super(node, "src/main/resources/pages/fn/menu-listing.txt",
				"src/main/resources/pages/fn/menu-listing-static.txt");
	}

}

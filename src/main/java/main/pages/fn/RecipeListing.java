package main.pages.fn;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class RecipeListing extends Page implements Validatable {
	
	public RecipeListing(Node node) {
		super(node, "src/main/resources/pages/fn/recipe-listing.txt",
				"src/main/resources/pages/fn/recipe-listing-static.txt");
	}

}

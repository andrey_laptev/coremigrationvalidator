package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVVideoChannelPage extends Page implements Validatable {
	
	public CCTVVideoChannelPage(Node node) {
		super(node, "src/main/resources/pages/cctv/videochannel.txt");
	}

}

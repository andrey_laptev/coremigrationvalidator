package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVBioPage extends Page implements Validatable {
	
	public CCTVBioPage(Node node) {
		super(node, "src/main/resources/pages/cctv/bio.txt");
	}

}

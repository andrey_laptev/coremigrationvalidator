package main.pages.cctv;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import main.pages.Page;
import main.Validatable;
import main.core.Global;
import main.core.PathConverter;

public class CCTVVideoPage extends Page implements Validatable {
	
	public CCTVVideoPage(Node node) {
		super(node, "src/main/resources/pages/cctv/video.txt",
				"src/main/resources/pages/cctv/video-static.txt");
	}
	
	@Override
	protected void setCurrentTestPath() {
		try {
			Global.setCurrentTestPath(
					PathConverter.getCCTVConvertedSingleVideoPath(node.getPath())
					 + "/jcr:content/");
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}
}

package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVShowRecipesPage extends Page implements Validatable {
	
	public CCTVShowRecipesPage(Node node) {
		super(node, "src/main/resources/pages/cctv/showrecipe.txt");
	}

}

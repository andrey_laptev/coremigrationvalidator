package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVEpisodeArchivePage extends Page implements Validatable {
	
	public CCTVEpisodeArchivePage(Node node) {
		super(node, "src/main/resources/pages/cctv/episodearchive.txt");
	}

}

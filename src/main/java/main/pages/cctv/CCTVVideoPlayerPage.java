package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVVideoPlayerPage extends Page implements Validatable {
	
	public CCTVVideoPlayerPage(Node node) {
		super(node, "src/main/resources/pages/cctv/videoplayer.txt");
	}

}

package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVArticlePage extends Page implements Validatable {
	
	public CCTVArticlePage(Node node) {
		super(node, "src/main/resources/pages/cctv/article.txt");
	}

}

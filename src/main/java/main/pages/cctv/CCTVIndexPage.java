package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVIndexPage extends Page implements Validatable {
	
	public CCTVIndexPage(Node node) {
		super(node, "src/main/resources/pages/cctv/index.txt");
	}

}

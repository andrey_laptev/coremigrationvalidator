package main.pages.cctv;

import main.Validatable;
import main.pages.Page;

import javax.jcr.Node;

public class CCTVRecipePage extends Page implements Validatable {
	
	public CCTVRecipePage(Node node) {
		super(node, "src/main/resources/pages/cctv/recipe.txt",
				"src/main/resources/pages/cctv/recipe-static.txt", true);
	}

}

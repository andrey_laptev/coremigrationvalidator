package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVTalentRecipePage extends Page implements Validatable {
	
	public CCTVTalentRecipePage(Node node) {
		super(node, "src/main/resources/pages/cctv/talentrecipe.txt");
	}

}

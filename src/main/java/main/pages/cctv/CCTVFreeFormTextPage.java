package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVFreeFormTextPage extends Page implements Validatable {
	
	public CCTVFreeFormTextPage(Node node) {
		super(node, "src/main/resources/pages/cctv/freeformtext.txt");
	}

}

package main.pages.cctv;

import main.Validatable;
import main.core.Config;
import main.core.Global;
import main.pages.Page;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

public class CCTVEpisodePage extends Page implements Validatable {

    public CCTVEpisodePage(Node node) {
        super(node, "src/main/resources/pages/cctv/episode.txt");
    }

    @Override
    protected void setCurrentTestPath() {
        try {
            String path = node.getPath().replace(Config.getGoldPath(), Config.getTestPath());
            String result = injectEpisodeNodeIntoPath(path, "shows");
            Global.setCurrentTestPath(result);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    private String injectEpisodeNodeIntoPath(String path, String keyWord) {
        int index = -1;

        String[] input = path.split("/");
        String result = "/" + input[1];

        for (int i = 2; i < input.length; i++) {

            if (input[i].contentEquals(keyWord)) {
                index = i;
            }

            if (index != -1 && i == index + 3) {
                result += "/episodes";
            }

            result += "/" + input[i];
        }

        return result;
    }

}

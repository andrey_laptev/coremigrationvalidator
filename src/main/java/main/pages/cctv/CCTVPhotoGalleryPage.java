package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVPhotoGalleryPage extends Page implements Validatable {
	
	public CCTVPhotoGalleryPage(Node node) {
		super(node, "src/main/resources/pages/cctv/photogallery.txt");
	}

}

package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVTopicPage extends Page implements Validatable {
	
	public CCTVTopicPage(Node node) {
		super(node, "src/main/resources/pages/cctv/topic.txt",
				"src/main/resources/pages/cctv/topic-static.txt");
	}

}

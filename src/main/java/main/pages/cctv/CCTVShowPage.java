package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVShowPage extends Page implements Validatable {
	
	public CCTVShowPage(Node node) {
		super(node, "src/main/resources/pages/cctv/show.txt",
				"src/main/resources/pages/cctv/show-static.txt");
	}

}

package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVTalentPage extends Page implements Validatable {
	
	public CCTVTalentPage(Node node) {
		super(node, "src/main/resources/pages/cctv/talent.txt",
				"src/main/resources/pages/cctv/talent-static.txt");
	}

}

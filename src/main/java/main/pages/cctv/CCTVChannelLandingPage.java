package main.pages.cctv;

import javax.jcr.Node;

import main.pages.Page;
import main.Validatable;

public class CCTVChannelLandingPage extends Page implements Validatable {
	
	public CCTVChannelLandingPage(Node node) {
		super(node, "src/main/resources/pages/cctv/channellanding.txt");
	}

}

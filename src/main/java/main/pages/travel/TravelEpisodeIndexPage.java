package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;

public class TravelEpisodeIndexPage extends Page implements Validatable {
    public TravelEpisodeIndexPage(Node node) {
        super(node, "src/main/resources/pages/travel/episode-index.txt","src/main/resources/pages/travel/episode-index-static.txt");
    }
}

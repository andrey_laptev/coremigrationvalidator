package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;

public class TravelCityTier2Page extends Page implements Validatable {
    public TravelCityTier2Page(Node node) {
        super(node, "src/main/resources/pages/travel/city-tier2.txt", "src/main/resources/pages/travel/city-tier2-static.txt");
    }
}


package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;

public class TravelProgramSchedulePage extends Page implements Validatable {

    public TravelProgramSchedulePage(Node node) {
        super(node, "src/main/resources/pages/travel/program-schedule.txt");
        }
}

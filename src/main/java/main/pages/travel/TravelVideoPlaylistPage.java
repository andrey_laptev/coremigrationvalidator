package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;


public class TravelVideoPlaylistPage extends Page implements Validatable {
    public TravelVideoPlaylistPage(Node node) {
        super(node, "src/main/resources/pages/travel/video-playlist.txt", "src/main/resources/pages/travel/video-playlist-static.txt");
    }
}

package main.pages.travel;

import main.Validatable;
import main.pages.Page;

import javax.jcr.Node;


public class TravelShowPage extends Page implements Validatable {
    public TravelShowPage(Node node) {
        super(node, "src/main/resources/pages/travel/show.txt");
    }

    @Override
    public void validateComplex() {
        validateAssetLinks();
    }
}




package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;


public class TravelPackagePage extends Page implements Validatable {
    public TravelPackagePage(Node node) {
        super(node, "src/main/resources/pages/travel/package.txt", "src/main/resources/pages/travel/package-static.txt");
    }
}

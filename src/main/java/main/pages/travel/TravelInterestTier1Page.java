package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;


public class TravelInterestTier1Page extends Page implements Validatable {
    public TravelInterestTier1Page(Node node) {
        super(node, "src/main/resources/pages/travel/interest-tier1.txt", "src/main/resources/pages/travel/interest-tier1-static.txt");
    }
}

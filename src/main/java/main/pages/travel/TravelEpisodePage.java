package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;

public class TravelEpisodePage extends Page implements Validatable {
    public TravelEpisodePage(Node node) {
        super(node, "src/main/resources/pages/travel/episode-page.txt","src/main/resources/pages/travel/episode-page-static.txt");
    }

    @Override
    public void validateComplex() {
        validateAssetLinks();
    }
}

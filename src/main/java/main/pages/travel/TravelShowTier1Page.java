package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;


public class TravelShowTier1Page extends Page implements Validatable {
    public TravelShowTier1Page(Node node) {
        super(node, "src/main/resources/pages/travel/show-tier1.txt", "src/main/resources/pages/travel/show-tier1-static.txt");
    }
}

package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;

public class TravelArticleTopNPage extends Page implements Validatable {
    public TravelArticleTopNPage(Node node) {
        super(node, "src/main/resources/pages/travel/article-topN.txt");
    }
}

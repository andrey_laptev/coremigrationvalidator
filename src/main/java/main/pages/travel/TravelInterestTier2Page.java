package main.pages.travel;

import main.Validatable;
import main.pages.Page;

import javax.jcr.Node;


public class TravelInterestTier2Page extends Page implements Validatable {
    public TravelInterestTier2Page(Node node) {
        super(node, "src/main/resources/pages/travel/interest-tier2.txt", "src/main/resources/pages/travel/interest-tier2-static.txt");
    }
}


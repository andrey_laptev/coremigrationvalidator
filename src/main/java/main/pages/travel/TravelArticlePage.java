package main.pages.travel;

import main.Validatable;
import main.pages.Page;

import javax.jcr.Node;

public class TravelArticlePage extends Page implements Validatable {
    public TravelArticlePage(Node node) {
        super(node, "src/main/resources/pages/travel/article.txt");
    }
}
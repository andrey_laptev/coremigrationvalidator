package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;

public class TravelUniversalLandingPage extends Page implements Validatable {
    public TravelUniversalLandingPage(Node node) {
        super(node, "src/main/resources/pages/travel/universal-landing.txt");
    }
}

package main.pages.travel;


import main.Validatable;
import main.core.Global;
import main.core.Sessions;
import main.pages.Page;
import main.reporter.Reporter;

import javax.jcr.Node;
import javax.jcr.Property;

public class TravelVideo extends Page implements Validatable {
    public TravelVideo(Node node) {
        super(node, "src/main/resources/pages/travel/sni-asset-video.txt","src/main/resources/pages/travel/sni-asset-video-static.txt");
    }

    @Override
    public void validateComplex() {
        validateAssetLinks();
    }

    @Override
    protected void addGlobalProperties() {}

    @Override
    public void validate() {
        validateSimple();
        validateStatic();

        String pageLinkPath = getPageLinkPath();
        if(pageLinkPath == null){
            Reporter.log(
                    "MISSING PROPERTY sni:pageLinks",
                    Global.getPageType(),
                    this.getClass().getSimpleName().equals(Global.getPageType()) ?
                            "" : this.getClass().getSimpleName(),
                    "",
                    "sni:pageLinks",
                    "",
                     getNodePath(),
                    "sni:pageLinks",
                    "EMPTY OR DOESN'T EXIST");
            return;
        }

        clearGlobalProperties();
        fillMap("src/main/resources/pages/travel/video.txt");
        fillStaticMap("src/main/resources/pages/travel/video-static.txt");

        Global.setCurrentTestPath(pageLinkPath + "/jcr:content");
        validateSimple();
        validateStatic();
    }

    private String getPageLinkPath(){
        String pageLinkPath = null;
        try{
            String nodePath = getNodePath();
            Property pageLinkProperty = Sessions.getTestSession().getNode(nodePath).getProperty("sni:pageLinks");
            if(pageLinkProperty.isMultiple()){
                pageLinkPath = pageLinkProperty.getValues()[0].getString();
            } else{
                pageLinkPath = pageLinkProperty.getValue().getString();
            }
        } catch(Exception ex){}
        return pageLinkPath;
    }

    private String getNodePath(){
        String nodePath = "";
        try{
            nodePath = node.getPath();
        } catch(Exception ex){
        }
        return nodePath.replace("/travelchannel/", "/travel/");
    }
}

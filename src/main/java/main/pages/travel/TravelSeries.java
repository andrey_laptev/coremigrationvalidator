package main.pages.travel;


import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;

public class TravelSeries extends Page implements Validatable {
    public TravelSeries(Node node) {
        super(node, "src/main/resources/pages/travel/series.txt");
    }

    @Override
    public void validateComplex() {
        validateAssetLinks();
    }
}
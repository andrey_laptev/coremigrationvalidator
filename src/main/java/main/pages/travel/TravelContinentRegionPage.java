package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;


public class TravelContinentRegionPage extends Page implements Validatable {
    public TravelContinentRegionPage(Node node) {
        super(node, "src/main/resources/pages/travel/continent-region.txt", "src/main/resources/pages/travel/continent-region-static.txt");
    }
}


package main.pages.travel;

import main.Validatable;
import main.pages.Page;

import javax.jcr.Node;


public class TravelOpenTemplatePage extends Page implements Validatable {
    public TravelOpenTemplatePage(Node node) {
        super(node, "src/main/resources/pages/travel/open-template.txt");
    }
}


package main.pages.travel;

import main.Validatable;
import main.pages.Page;

import javax.jcr.Node;


public class TravelPhotoGalleryPage extends Page implements Validatable {
    public TravelPhotoGalleryPage(Node node) {
        super(node, "src/main/resources/pages/travel/photo-gallery.txt", "src/main/resources/pages/travel/photo-gallery-static.txt");
    }
}


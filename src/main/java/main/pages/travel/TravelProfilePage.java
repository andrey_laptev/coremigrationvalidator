package main.pages.travel;

import main.Validatable;
import main.core.Global;
import main.core.Sessions;
import main.pages.Page;
import main.reporter.Reporter;

import javax.jcr.Node;


public class TravelProfilePage extends Page implements Validatable {
    public TravelProfilePage(Node node) {
        super(node, "src/main/resources/pages/travel/profile.txt", "src/main/resources/pages/travel/profile-static.txt");
    }

    @Override
    public void validateComplex() {
       try{
           String path = Global.getCurrentTestPath()+"image";
           if(!Sessions.getTestSession().nodeExists(path) || !Sessions.getTestSession().getNode(path).hasProperty("fileReference")){
               return;
           } else{
               if(!Sessions.getTestSession().getNode(Global.getCurrentTestPath()).hasProperty("avatarPath") )
               {
                   Reporter.log(
                           "MISSING PROPERTY avatarPath",
                           Global.getPageType(),
                           this.getClass().getSimpleName().equals(Global.getPageType()) ?
                                   "" : this.getClass().getSimpleName(),
                           "",
                           "avatarPath",
                           "",
                           Global.getCurrentTestPath(),
                           "avatarPath",
                           "EMPTY OR DOESN'T EXIST");
                   return;
               }
           }
       } catch(Exception ex){}
    }
}

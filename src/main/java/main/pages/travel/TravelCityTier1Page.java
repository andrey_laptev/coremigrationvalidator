package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;

public class TravelCityTier1Page extends Page implements Validatable {
    public TravelCityTier1Page(Node node) {
        super(node, "src/main/resources/pages/travel/city-tier1.txt", "src/main/resources/pages/travel/city-tier1-static.txt");
    }
}

package main.pages.travel;

import main.Validatable;
import main.pages.Page;

import javax.jcr.Node;


public class TravelVideoCollection extends Page implements Validatable {

    public TravelVideoCollection(Node node) {
        super(node, "src/main/resources/pages/travel/video-collection.txt", "src/main/resources/pages/travel/video-collection-static.txt");
    }
}

package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;


public class TravelShowTier2Page extends Page implements Validatable {
    public TravelShowTier2Page(Node node) {
        super(node, "src/main/resources/pages/travel/show-tier2.txt", "src/main/resources/pages/travel/show-tier2-static.txt");
    }
}
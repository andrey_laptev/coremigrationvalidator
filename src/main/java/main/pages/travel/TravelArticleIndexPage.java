package main.pages.travel;

import main.Validatable;
import main.core.Config;
import main.core.Global;
import main.core.Sessions;
import main.pages.Page;
import main.reporter.Reporter;

import javax.jcr.Node;
import javax.jcr.Session;

public class TravelArticleIndexPage extends Page implements Validatable {
    public TravelArticleIndexPage(Node node) {
        super(node, "src/main/resources/pages/travel/article-index.txt","src/main/resources/pages/travel/article-index-static.txt");
    }


    @Override
    public void validateComplex() {
        verifyLandingPages("listArticle","/articles/");
    }
}


package main.pages.travel;

import main.Validatable;
import main.pages.Page;

import javax.jcr.Node;

public class TravelPhotoIndexPage extends Page implements Validatable {
    public TravelPhotoIndexPage(Node node) {
        super(node, "src/main/resources/pages/travel/photo-index.txt", "src/main/resources/pages/travel/photo-index-static.txt");
    }

    @Override
    public void validateComplex() {
        verifyLandingPages("listPhotoGallery","/photos/");
    }
}

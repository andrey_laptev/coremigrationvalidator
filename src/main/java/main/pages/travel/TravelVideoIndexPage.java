package main.pages.travel;

import main.Validatable;
import main.pages.Page;
import javax.jcr.Node;


public class TravelVideoIndexPage extends Page implements Validatable {
    public TravelVideoIndexPage(Node node) {
        super(node, "src/main/resources/pages/travel/video-index.txt", "src/main/resources/pages/travel/video-index-static.txt");
    }

    @Override
    public void validateComplex() {
        verifyLandingPages("listVideo","/videos/");
    }
}
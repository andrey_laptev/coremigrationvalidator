package main;

import main.core.Config;
import main.modules.travel.*;
import main.pages.travel.*;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;


public class TravelRunner {

    public static Validatable getValidatable(Node node)
            throws ValueFormatException, RepositoryException {

        if (!node.hasProperty("sling:resourceType")) {
            return null;
        }

        // travel
        //pages
        final String travelShowTier2Page = "tccom/components/pages/showPage/showPageTier2";
        final String travelShowTier1Page = "tccom/components/pages/showPage/showPageTier1";
        final String travelOpenTemplatePage = "tccom/components/pages/openTemplatePage";
        final String travelEpisodePage = "tccom/components/pages/episodePage";
        final String travelPackagePage = "tccom/components/pages/packagePage";
        final String travelCityTier2Page = "tccom/components/pages/destinationPage/cityPageTier2";
        final String travelCityTier1Page = "tccom/components/pages/destinationPage/cityPageTier1";
        final String travelArticlePage = "tccom/components/pages/articlePage";
        final String travelProgramSchedulePage = "tccom/components/pages/programSchedulePage";
        final String travelInterestTier1Page = "tccom/components/pages/interestPage/interestPageTier1";
        final String travelInterestTier2Page = "tccom/components/pages/interestPage/interestPageTier2";
        final String travelPhotoGalleryPage = "tccom/components/pages/photoGalleryPage";
        final String travelArticleTopNPage = "tccom/components/pages/articleTopNPage";
        final String travelContinentRegionPage = "tccom/components/pages/destinationPage/continentRegionPage";
        final String travelProfilePage = "tccom/components/pages/profilePage";
        final String travelPhotoIndexPage = "tccom/components/pages/photoIndexPage";
        final String travelArticleIndexPage = "tccom/components/pages/articleIndexPage";
        final String travelVideoIndexPage = "tccom/components/pages/videoIndexPage";
        final String travelEpisodeIndexPage = "tccom/components/pages/episodeIndexPage";
        final String travelUniversalLandingPage = "tccom/components/pages/universalLandingPage";
        final String travelShowPage = "tccom/components/pages/showPage";
        final String travelVideoPlaylistPage ="travelcom/components/pages/videoPlaylistPage";
        final String countryStatePage = "tccom/components/pages/destinationPage/countryStatePage";
        final String travelVideoCollection = "tccom/components/pages/videoCollectionPage";

        //modules
        final String travelAssetTitle = "tccom/components/article/assetTitle";
        final String travelAssetDescription = "tccom/components/article/assetDescription";
        final String travelAttribution = "tccom/components/article/attribution";
        final String travelRte = "tccom/components/article/customRTE";
        final String travelInnerRail = "tccom/components/article/innerRail";
        final String travelImageEmbed = "tccom/components/article/imageEmbed";
        final String travelNextUp = "tccom/components/article/nextUp";
        final String travelSingleVideoEmbed = "tccom/components/article/videoPlayerEmbed";
        final String travelPullQuote = "tccom/components/article/pullQuote";
        final String travelJukebox = "tccom/components/general/jukebox";
        final String travelInterestLead = "tccom/components/general/jukebox/lead/interestLead";
        final String travelProfileLead = "tccom/components/general/jukebox/lead/profileLead";
        final String travelShowLead = "tccom/components/general/jukebox/lead/showLead";
        final String travelDestinationLead = "tccom/components/general/jukebox/lead/destinationLead";
        final String travelEditorialPromo = "tccom/components/article/editorialPromo";
        final String travelMediaStrip ="tccom/components/general/jukebox/mediaStrip";
        final String travelCarouselPromo = "tccom/components/general/jukebox/carouselPromo/travelChannelVisits";
        final String travelDestinationBanner = "tccom/components/article/banner/destinationsBanner";
        final String travelSubNav = "/jcr:content/superLeadParsys/banner/bannerSubNav";
        final String travelPhotoCollage = "tccom/components/general/jukebox/photoCollage";
        final String travelProfilePromo = "tccom/components/general/profilePromo";
        final String travelEpisodeFeed = "tccom/components/general/episodeFeed";
        final String travelBulletedList = "tccom/components/article/bulletList";
        final String travelProfileBanner = "tccom/components/article/banner/profileBanner";
        final String travelVideoPlaylistEmbed = "tccom/components/general/jukebox/videoPlaylistEmbed";
        final String travelVoting = "tccom/components/general/voting";
        final String travelAssetIntro = "tccom/components/article/assetIntro";
        final String videoPlaylistEmbedJukebox = "tccom/components/general/jukebox/videoPlaylistEmbed";
        final String travelEpisodePagination = "tccom/components/general/episodePagination";
        final String travelEpisodeTuneIn = "tccom/components/hidden/episodeTuneIn";
        final String travelVideo = "tccom/components/pages/mediadeskVideo";
        final String travelSeries = "tccom/components/pages/seasonPage";
        final String travelVideoPlaylist = "tccom/components/general/jukebox/videoPlaylist";

        final String travelMediaNet = "tccom/components/article/mediaTag";
        final String travelMoreFrom = "tccom/components/article/moreFrom";
        final String travelInstagramEmbed = "tccom/components/article/socialEmbed/instagramEmbed";
        final String travelPinterestPinEmbed = "tccom/components/article/socialEmbed/pinterestEmbed";
        final String travelVineEmbed = "tccom/components/article/socialEmbed/vineEmbed";
        final String travelPinterestBoard = "tccom/components/article/socialEmbed/profileEmbed";
        final String travelAboutTheShop = "tccom/components/general/aboutTheShow";
        final String travelFreeFromHtml = "tccom/components/html";
        final String travelDestinationSubNav= "tccom/components/hidden/subNavigation/destinationsSubNav";
        final String travelShowsBanner = "tccom/components/article/banner/showBanner";
        final String travelTags  = "tccom/components/article/tags";


        String resourceType = node.getProperty("sling:resourceType").getString();

        //travel
        if (travelShowTier2Page.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("showPageTier2") ? new TravelShowTier2Page(node) : null;
        }
        if (travelShowTier1Page.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("showPageTier1") ? new TravelShowTier1Page(node) : null;
        }
        if (travelArticleIndexPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("articleIndexPage") ? new TravelArticleIndexPage(node) : null;
        }
        if(travelArticlePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("articlePage") ? new TravelArticlePage(node) : null;
        }
        if(travelArticleTopNPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("articleTopNPage") ? new TravelArticleTopNPage(node) : null;
        }
        if(travelCityTier1Page.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("cityPageTier1") ? new TravelCityTier1Page(node) : null;
        }
        if(travelCityTier2Page.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("cityPageTier2") ? new TravelCityTier2Page(node) : null;
        }
        if(travelContinentRegionPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("continentRegionPage") ? new TravelContinentRegionPage(node) : null;
        }
        if(travelEpisodeIndexPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("episodeIndexPage") ? new TravelEpisodeIndexPage(node) : null;
        }
        if(travelEpisodePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("episodePage") ? new TravelEpisodePage(node) : null;
        }
        if(travelInterestTier1Page.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("interestPageTier1") ? new TravelInterestTier1Page(node) : null;
        }
        if(travelInterestTier2Page.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("interestPageTier2") ? new TravelInterestTier2Page(node) : null;
        }
        if(travelOpenTemplatePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("openTemplatePage") ? new TravelOpenTemplatePage(node) : null;
        }
        if(travelPackagePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("packagePage") ? new TravelPackagePage(node) : null;
        }
        if(travelPhotoGalleryPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("photoGalleryPage") ? new TravelPhotoGalleryPage(node) : null;
        }
        if(travelPhotoIndexPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("photoIndexPage") ? new TravelPhotoIndexPage(node) : null;
        }
        if(travelProfilePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("profilePage") ? new TravelProfilePage(node) : null;
        }
        if(travelProgramSchedulePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("programSchedulePage") ? new TravelProgramSchedulePage(node) : null;
        }
        if(travelShowPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("showPage") ? new TravelShowPage(node) : null;
        }
        if(travelShowTier1Page.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("showPageTier1") ? new TravelShowTier1Page(node) : null;
        }
        if(travelShowTier2Page.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("showPageTier2") ? new TravelShowTier2Page(node) : null;
        }
        if(travelUniversalLandingPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("universalLandingPage") ? new TravelUniversalLandingPage(node) : null;
        }
        if(travelVideoIndexPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("videoIndexPage") ? new TravelVideoIndexPage(node) : null;
        }
        if(travelVideoPlaylistPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("videoPlaylistPage") ? new TravelVideoPlaylistPage(node) : null;
        }
        if(countryStatePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("countryStatePage") ? new TravelContinentRegionPage(node) : null;
        }
        if(travelVideo.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("mediadeskVideo") ? new TravelVideo(node) : null;
        }
        if(travelSeries.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("series") ? new TravelSeries(node) : null;
        }
        if(travelVideoCollection.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("videoCollection") ?  new TravelVideoCollection(node) : null;
        }


        //Modules
        //travel
        if (travelAssetDescription.equalsIgnoreCase(resourceType)) {
            return new AssetDescription(node);
        }
        if (travelAssetTitle.equalsIgnoreCase(resourceType)) {
            return new AssetTitle(node);
        }
        if (travelAttribution.equalsIgnoreCase(resourceType)) {
            return new Attribution(node);
        }
        if (travelImageEmbed.equalsIgnoreCase(resourceType)) {
            return new ImageEmbed(node);
        }
        if (travelInnerRail.equalsIgnoreCase(resourceType)) {
            return new InnerRail(node);
        }
        if (travelNextUp.equalsIgnoreCase(resourceType)) {
            return new NextUp(node);
        }
        if (travelSingleVideoEmbed.equalsIgnoreCase(resourceType)) {
            return new SingleVideoEmbed(node);
        }
        if (travelPullQuote.equalsIgnoreCase(resourceType)) {
            return new PullQuote(node);
        }
        if (travelRte.equalsIgnoreCase(resourceType)) {
            return new SmartRte(node);
        }
        if(travelJukebox.equalsIgnoreCase(resourceType)){
            return  new Jukebox(node);
        }
        if (travelInterestLead.equalsIgnoreCase(resourceType)){
            return new InterestLead(node);
        }
        if (travelProfileLead.equalsIgnoreCase(resourceType)){
            return new ProfileLead(node);
        }
        if (travelShowLead.equalsIgnoreCase(resourceType)){
            return new ShowLead(node);
        }
        if(travelDestinationLead.equalsIgnoreCase(resourceType)){
            return new CityLead(node);
        }
        if(travelEditorialPromo.equalsIgnoreCase(resourceType)){
            return new EditorialPromo(node);
        }
        if(travelMediaStrip.equalsIgnoreCase(resourceType)){
            return new MediaStrip(node);
        }
        if(travelCarouselPromo.equalsIgnoreCase(resourceType)){
            return new CarouselPromo(node);
        }
        if(travelDestinationBanner.equalsIgnoreCase(resourceType)){
            return new DestinationBanner(node);
        }
        if(travelSubNav.equalsIgnoreCase(resourceType)){
            return new SubNavigator(node);
        }
        if(travelPhotoCollage.equalsIgnoreCase(resourceType)){
            return new PhotoCollage(node);
        }
        if(travelProfilePromo.equalsIgnoreCase(resourceType)){
            return new ProfilePromo(node);
        }
        if(travelEpisodeFeed.equalsIgnoreCase(resourceType)){
            return new EpisodeFeed(node);
        }
        if(travelBulletedList.equalsIgnoreCase(resourceType)){
            return new BulletedList(node);
        }
        if(travelProfileBanner.equalsIgnoreCase(resourceType)){
            return new ProfileBanner(node);
        }
        if(travelVideoPlaylistEmbed.equalsIgnoreCase(resourceType)){
            return new VideoPlaylistEmbed(node);
        }
        if(travelVoting.equalsIgnoreCase(resourceType)){
            return new Voting(node);
        }
        if(travelAssetIntro.equalsIgnoreCase(resourceType)){
            return new AssetIntro(node);
        }
        if(travelShowsBanner.equalsIgnoreCase(resourceType)){
            return new ShowBanner(node);
        }
        if(videoPlaylistEmbedJukebox.equalsIgnoreCase(resourceType)){
            return new VideoPlaylistEmbedJukebox(node);
        }
        if(travelAboutTheShop.equalsIgnoreCase(resourceType)){
            return new AboutTheShow(node);
        }
        if(travelEpisodePagination.equalsIgnoreCase(resourceType)){
            return new EpisodePagination(node);
        }
        if(travelEpisodeTuneIn.equalsIgnoreCase(resourceType)){
            return new EpisodeTuneIn(node);
        }
        if(travelVideoPlaylist.equalsIgnoreCase(resourceType)){
            return new VideoPlaylist(node);
        }

        return null;
    }


}

package main.modules.travel;

import main.ContentNode;
import main.Validatable;
import javax.jcr.Node;

public class AssetTitle extends ContentNode implements Validatable {

    public AssetTitle(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/asset-title.txt");
        fillStaticMap("src/main/resources/components/travel/asset-title-static.txt");
    }
}

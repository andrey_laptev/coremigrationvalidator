package main.modules.travel;

import main.ContentNode;
import main.Validatable;
import javax.jcr.Node;

public class SmartRte extends ContentNode implements Validatable {

    public SmartRte(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/smart-rte.txt");
        fillStaticMap("src/main/resources/components/travel/smart-rte-static.txt");
    }
}

package main.modules.travel;

import main.ContentNode;
import main.Validatable;
import main.core.Global;
import main.reporter.Reporter;
import main.rules.StaticMappingRule;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import java.util.List;


public class Jukebox extends ContentNode implements Validatable {

    public Jukebox(Node node) {
        this.node = node;
        if (Global.getPageType().equalsIgnoreCase("TravelEpisodePage")){
            fillMap("src/main/resources/components/travel/jukebox_episode_page.txt");
            fillStaticMap("src/main/resources/components/travel/jukebox_episode_page-static.txt");
        } else{
            fillMap("src/main/resources/components/travel/jukebox.txt");
            fillStaticMap("src/main/resources/components/travel/jukebox-static.txt");
        }
    }


    @Override
    protected void validateStatic() {
        validateDynamicStatic();
    }
}
package main.modules.travel;


import main.ContentNode;
import main.Validatable;
import javax.jcr.Node;

public class Attribution extends ContentNode implements Validatable {

    public Attribution(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/attribution.txt");
        fillStaticMap("src/main/resources/components/travel/attribution-static.txt");
    }
}

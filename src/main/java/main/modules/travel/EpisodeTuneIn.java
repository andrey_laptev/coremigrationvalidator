package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;

public class EpisodeTuneIn extends ContentNode implements Validatable {

    public EpisodeTuneIn(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/episode-tunein.txt");
        fillStaticMap("src/main/resources/components/travel/episode-tunein-static.txt");
    }
}
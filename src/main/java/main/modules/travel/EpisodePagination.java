package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;


public class EpisodePagination extends ContentNode implements Validatable {

    public EpisodePagination(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/episode-pagination.txt");
        fillStaticMap("src/main/resources/components/travel/episode-pagination-static.txt");
    }
}
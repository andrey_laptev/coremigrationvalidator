package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;

public class NextUp extends ContentNode implements Validatable {

    public NextUp(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/next-up.txt");
        fillStaticMap("src/main/resources/components/travel/next-up-static.txt");
    }
}

package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;


public class ProfilePromo extends ContentNode implements Validatable {

    public ProfilePromo(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/profile-promo.txt");
        fillStaticMap("src/main/resources/components/travel/profile-promo-static.txt");
    }
}
package main.modules.travel;


import main.ContentNode;
import main.Validatable;
import main.core.Global;

import javax.jcr.Node;

public class VideoPlaylistEmbedJukebox extends ContentNode implements Validatable {

    public VideoPlaylistEmbedJukebox(Node node) {
        this.node = node;
        if (!Global.getPageType().equalsIgnoreCase("TravelPackagePage")) {
            fillMap("src/main/resources/components/travel/video-playlist-embed.txt");
            fillStaticMap("src/main/resources/components/travel/video-playlist-embed-jukebox-static.txt");
        }
    }
}

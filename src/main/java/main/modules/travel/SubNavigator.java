package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;

public class SubNavigator extends ContentNode implements Validatable {

    public SubNavigator(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/sub-navigator.txt");
        fillStaticMap("src/main/resources/components/travel/sub-navigator-static.txt");
    }
}
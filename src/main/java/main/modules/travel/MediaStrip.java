package main.modules.travel;

import main.ContentNode;
import main.Validatable;
import main.core.Global;

import javax.jcr.Node;

public class MediaStrip extends ContentNode implements Validatable {

    public MediaStrip(Node node) {
        this.node = node;
        if(Global.getPageType().equalsIgnoreCase("TravelPhotoGalleryPage") ||
                Global.getPageType().equalsIgnoreCase("TravelArticlePage") ||
                Global.getPageType().equalsIgnoreCase("TravelPackagePage")){
            return;
        }
        fillMap("src/main/resources/components/travel/media-strip.txt");

        if(Global.getPageType().equalsIgnoreCase("TravelEpisodePage")){
            fillStaticMap( "src/main/resources/components/travel/media-strip_episode_page-static.txt");
        } else{
            fillStaticMap("src/main/resources/components/travel/media-strip-static.txt");
        }
    }

    @Override
    protected void validateStatic() {
        validateDynamicStatic();
    }
}
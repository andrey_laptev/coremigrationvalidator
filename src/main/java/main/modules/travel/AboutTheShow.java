package main.modules.travel;


import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;

public class AboutTheShow extends ContentNode implements Validatable {

    public AboutTheShow(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/about-the-show.txt");
        fillStaticMap("src/main/resources/components/travel/about-the-show-static.txt");
    }

}
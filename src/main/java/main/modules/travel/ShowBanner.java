package main.modules.travel;


import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;

public class ShowBanner extends ContentNode implements Validatable {

    public ShowBanner(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/show-banner.txt");
        fillStaticMap("src/main/resources/components/travel/show-banner-static.txt");
    }
}
package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;

public class DestinationBanner extends ContentNode implements Validatable {

    public DestinationBanner(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/destination-banner.txt");
        fillStaticMap("src/main/resources/components/travel/destination-banner-static.txt");
    }
}

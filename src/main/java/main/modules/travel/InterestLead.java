package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;


public class InterestLead extends ContentNode implements Validatable {

    public InterestLead(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/interest-lead.txt");
        fillStaticMap("src/main/resources/components/travel/interest-lead-static.txt");
    }
}


package main.modules.travel;

import main.ContentNode;
import main.Validatable;
import main.core.Global;

import javax.jcr.Node;

public class ShowLead extends ContentNode implements Validatable {

    public ShowLead(Node node) {
        this.node = node;
        if (Global.getPageType().equalsIgnoreCase("TravelShowTier1Page") ||
                Global.getPageType().equalsIgnoreCase("TravelShowTier2Page")){
            fillMap("src/main/resources/components/travel/show_shows-lead.txt");
            fillStaticMap("src/main/resources/components/travel/show-lead_shows-static.txt");
        } else{
            fillMap("src/main/resources/components/travel/show-lead.txt");
            fillStaticMap("src/main/resources/components/travel/show-lead-static.txt");
        }
    }
}

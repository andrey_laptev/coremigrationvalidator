package main.modules.travel;


import main.ContentNode;
import main.Validatable;
import javax.jcr.Node;

public class SingleVideoEmbed extends ContentNode implements Validatable {

    public SingleVideoEmbed(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/single-video-embed.txt");
        fillStaticMap("src/main/resources/components/travel/single-video-embed-static.txt");
    }
}

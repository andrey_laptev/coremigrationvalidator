package main.modules.travel;


import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;

public class PullQuote extends ContentNode implements Validatable {

    public PullQuote(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/pull-quote.txt");
        fillStaticMap("src/main/resources/components/travel/pull-quote-static.txt");
    }
}

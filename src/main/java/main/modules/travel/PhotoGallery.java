package main.modules.travel;


import main.ContentNode;
import main.Validatable;
import javax.jcr.Node;

public class PhotoGallery extends ContentNode implements Validatable {

    public PhotoGallery(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/photo-gallery.txt");
        fillStaticMap("src/main/resources/components/travel/photo-gallery-static.txt");
    }


    @Override
    protected void validateStatic() {
        validateDynamicStatic();
    }
}

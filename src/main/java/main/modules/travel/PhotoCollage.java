package main.modules.travel;

import main.ContentNode;
import main.Validatable;
import main.core.Global;

import javax.jcr.Node;


public class PhotoCollage extends ContentNode implements Validatable {

    public PhotoCollage(Node node) {
        this.node = node;
        if (!Global.getPageType().equalsIgnoreCase("TravelPhotoGalleryPage") ||
            !Global.getPageType().equalsIgnoreCase("TravelPackagePage")){
            fillMap("src/main/resources/components/travel/photo-collage.txt");
            fillStaticMap("src/main/resources/components/travel/photo-collage-static.txt");
        }
    }
}
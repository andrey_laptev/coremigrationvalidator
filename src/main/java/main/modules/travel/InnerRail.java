package main.modules.travel;


import main.ContentNode;
import main.Validatable;
import javax.jcr.Node;

public class InnerRail extends ContentNode implements Validatable {

    public InnerRail(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/inner-rail.txt");
        fillStaticMap("src/main/resources/components/travel/inner-rail-static.txt");
    }
}

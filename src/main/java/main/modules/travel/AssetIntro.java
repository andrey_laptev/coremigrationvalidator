package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;


public class AssetIntro extends ContentNode implements Validatable {

    public AssetIntro(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/asset-intro.txt");
        fillStaticMap("src/main/resources/components/travel/asset-intro-static.txt");
    }

}

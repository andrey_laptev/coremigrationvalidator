package main.modules.travel;

import main.ContentNode;
import main.Validatable;
import main.core.Global;
import main.core.Sessions;
import main.reporter.Reporter;

import javax.jcr.Node;
import javax.jcr.Session;


public class EditorialPromo  extends ContentNode implements Validatable {

    public EditorialPromo(Node node) {
        this.node = node;
        if (!Global.getPageType().equalsIgnoreCase("TravelEpisodePage")) {
            fillMap("src/main/resources/components/travel/editorial-promo.txt");
            fillStaticMap("src/main/resources/components/travel/editorial-promo-static.txt");
        } else{
            validateDeletion();
        }
    }

    public void validateDeletion() {
        try {
            String extraPath = node.getPath().substring(node.getPath().lastIndexOf("jcr:content") + 12);
            if (Sessions.getTestSession().nodeExists(Global.getCurrentTestPath()+ extraPath) ||
                    Sessions.getTestSession().nodeExists(Global.getCurrentTestPath()+ "articleBody/editorialpromo") ||
                      Sessions.getTestSession().nodeExists(Global.getCurrentTestPath()+ "articleBody/editorialPromo")) {
                Reporter.log(
                        "EDITORIAL PROMO WAS CREATED",
                        Global.getPageType(),
                        this.getClass().getSimpleName().equals(Global.getPageType()) ?
                                "" : this.getClass().getSimpleName(),
                        node.getPath(),
                        "editorialPromo",
                        "",
                        Global.getCurrentTestPath(),
                        "editorialPromo",
                        "");
                }
        } catch(Exception ex){}
    }
}

package main.modules.travel;


import main.ContentNode;
import main.Validatable;
import javax.jcr.Node;

public class ProfileLead extends ContentNode implements Validatable {

    public ProfileLead(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/profile-lead.txt");
        fillStaticMap("src/main/resources/components/travel/profile-lead-static.txt");
    }
}
package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;


public class CityLead extends ContentNode implements Validatable {

    public CityLead(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/city-lead.txt");
        fillStaticMap("src/main/resources/components/travel/city-lead-static.txt");
    }

    @Override
    protected void validateStatic() {
        validateDynamicStatic();
    }
}

package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;

public class EpisodeFeed extends ContentNode implements Validatable {

    public EpisodeFeed(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/episode-feed.txt");
        fillStaticMap("src/main/resources/components/travel/episode-feed-static.txt");
    }
}
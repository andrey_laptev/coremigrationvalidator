package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;

public class CarouselPromo  extends ContentNode implements Validatable {

    public CarouselPromo(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/carousel-promo.txt");
        fillStaticMap("src/main/resources/components/travel/carousel-promo-static.txt");
    }
}
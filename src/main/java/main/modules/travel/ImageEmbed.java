package main.modules.travel;


import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;

public class ImageEmbed extends ContentNode implements Validatable {

    public ImageEmbed(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/image-embed.txt");
        fillStaticMap("src/main/resources/components/travel/image-embed-static.txt");
    }
}

package main.modules.travel;

import main.ContentNode;
import main.Validatable;
import javax.jcr.Node;

public class AssetDescription extends ContentNode implements Validatable {

    public AssetDescription(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/asset-description.txt");
        fillStaticMap("src/main/resources/components/travel/asset-description-static.txt");
    }

}

package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;


public class ProfileBanner extends ContentNode implements Validatable {

    public ProfileBanner(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/profile-banner.txt");
        fillStaticMap("src/main/resources/components/travel/profile-banner-static.txt");
    }
}

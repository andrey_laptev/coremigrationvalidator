package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;


public class BulletedList  extends ContentNode implements Validatable {

    public BulletedList(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/bulleted-list.txt");
        fillStaticMap("src/main/resources/components/travel/bulleted-list-static.txt");
    }
}
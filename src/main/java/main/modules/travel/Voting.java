package main.modules.travel;

import main.ContentNode;
import main.Validatable;

import javax.jcr.Node;

public class Voting extends ContentNode implements Validatable {

    public Voting(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/voting.txt");
        fillStaticMap("src/main/resources/components/travel/voting-static.txt");
    }
}
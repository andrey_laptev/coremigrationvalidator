package main.modules.travel;

import main.ContentNode;
import main.Validatable;
import main.core.Global;
import main.core.Sessions;
import main.reporter.Reporter;

import javax.jcr.Node;
import javax.jcr.NodeIterator;


public class VideoPlaylist extends ContentNode implements Validatable {

    public VideoPlaylist(Node node) {
        this.node = node;
        fillMap("src/main/resources/components/travel/video-playlist.txt");
        fillStaticMap("src/main/resources/components/travel/video-playlist-static.txt");
    }

    @Override
    protected void validateComplex(){
       try{
           if(!node.hasProperty("type") || !node.getProperty("type").getValue().getString().contains("curated")){
               return;
           }

           NodeIterator nodeIterator = Sessions.getGoldSession()
                   .getNode(node.getPath()).getNodes();

           while (nodeIterator.hasNext()){
               Node currentNode = nodeIterator.nextNode();
               validateSingleNode(currentNode);
           }

       } catch(Exception ex){
           Reporter.log(
                   "FAILED WITH EXCEPTION",
                   Global.getPageType(),
                   this.getClass().getSimpleName().equals(Global.getPageType()) ?
                           "" : this.getClass().getSimpleName(),
                   "",
                   "",
                   "",
                   Global.getCurrentTestPath(),
                   "",
                   "");
       }
    }

    protected void validateSingleNode(Node currentNode){
        try{

            String [] bits = currentNode.getPath().split("-");
            String testPath = Global.getCurrentTestPath() + "videoPlaylistPromo/videos-" + bits[bits.length-1];
            if(!Sessions.getTestSession().nodeExists(testPath)){
                Reporter.log(
                        "MISSING NODE",
                        Global.getPageType(),
                        this.getClass().getSimpleName().equals(Global.getPageType()) ?
                                "" : this.getClass().getSimpleName(),
                        currentNode.getPath(),
                        "",
                        "",
                        testPath,
                        "",
                        "EMPTY OR DOESN'T EXIST");
                return;
            }
            Node testNode = Sessions.getTestSession().getNode(testPath);

            if(currentNode.hasProperty("resourceReference")){
                if(!testNode.hasProperty("video")){
                    Reporter.log(
                            "MISSING PROPERTY video",
                            Global.getPageType(),
                            this.getClass().getSimpleName().equals(Global.getPageType()) ?
                                    "" : this.getClass().getSimpleName(),
                            currentNode.getPath(),
                            "resourceReference",
                            "",
                            testPath,
                            "video",
                            "EMPTY OR DOESN'T EXIST");
                    return;
                }

                String video = testNode.getProperty("video").getValue().getString();
                if(!Sessions.getTestSession().nodeExists(video)){
                    Reporter.log(
                            "MISSING NODE video",
                            Global.getPageType(),
                            this.getClass().getSimpleName().equals(Global.getPageType()) ?
                                    "" : this.getClass().getSimpleName(),
                            currentNode.getPath(),
                            "resourceReference",
                            "",
                            testPath,
                            "video",
                            video);
                }
            } else if(currentNode.hasProperty("descriptionOverride")){

                if(currentNode.getProperty("descriptionOverride").getValue().getString() != testNode.getProperty("customDescription").getValue().getString()){
                    Reporter.log(
                            "PROPERTY VALUE MISMATCH descriptionOverride",
                            Global.getPageType(),
                            this.getClass().getSimpleName().equals(Global.getPageType()) ?
                                    "" : this.getClass().getSimpleName(),
                            currentNode.getPath(),
                            "descriptionOverride",
                            currentNode.getProperty("descriptionOverride").getValue().getString(),
                            testPath,
                            "customDescription",
                            testNode.getProperty("customDescription").getValue().getString());
                }

            } else if(currentNode.hasProperty("text")){
                if(currentNode.getProperty("text").getValue().getString() != testNode.getProperty("customTitle").getValue().getString()){
                    Reporter.log(
                            "PROPERTY VALUE MISMATCH text",
                            Global.getPageType(),
                            this.getClass().getSimpleName().equals(Global.getPageType()) ?
                                    "" : this.getClass().getSimpleName(),
                            currentNode.getPath(),
                            "text",
                            currentNode.getProperty("text").getValue().getString(),
                            testPath,
                            "customTitle",
                            testNode.getProperty("customTitle").getValue().getString());
                }
            }
        } catch(Exception ex){
            Reporter.log(
                    "FAILED WITH EXCEPTION",
                    Global.getPageType(),
                    this.getClass().getSimpleName().equals(Global.getPageType()) ?
                            "" : this.getClass().getSimpleName(),
                    "",
                    "",
                    "",
                    Global.getCurrentTestPath(),
                    "",
                    "");
        }
    }
}

package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVPrimaryTalent extends ContentNode implements Validatable {
	
	public CCTVPrimaryTalent(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/primary-talent.txt");
		fillStaticMap("src/main/resources/components/cctv/primary-talent-static.txt");
	}

}

package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVArticleSimpleTitle extends ContentNode implements Validatable {
	
	public CCTVArticleSimpleTitle(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/article-simple-title.txt");
	}

}

package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVTopic extends ContentNode implements Validatable {
	
	public CCTVTopic(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/topic.txt");
	}

}

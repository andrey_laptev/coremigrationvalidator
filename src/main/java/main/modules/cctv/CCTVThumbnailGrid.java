package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVThumbnailGrid extends ContentNode implements Validatable {
	
	public CCTVThumbnailGrid(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/thumbnail_grid.txt");
	}
}

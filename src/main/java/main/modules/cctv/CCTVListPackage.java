package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVListPackage extends ContentNode implements Validatable {
	
	public CCTVListPackage(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/list-package.txt");
	}

}

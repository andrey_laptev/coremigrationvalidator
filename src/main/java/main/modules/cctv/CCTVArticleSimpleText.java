package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVArticleSimpleText extends ContentNode implements Validatable {
	
	public CCTVArticleSimpleText(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/article-simple-text.txt");
	}

}

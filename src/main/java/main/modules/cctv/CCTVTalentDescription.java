package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVTalentDescription extends ContentNode implements Validatable {
	
	public CCTVTalentDescription(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/talent-description.txt");
		fillStaticMap("src/main/resources/components/cctv/talent-description-static.txt");
	}


}

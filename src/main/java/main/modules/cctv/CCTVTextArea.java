package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVTextArea extends ContentNode implements Validatable {
	
	public CCTVTextArea(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/text-area.txt");
	}
 

}

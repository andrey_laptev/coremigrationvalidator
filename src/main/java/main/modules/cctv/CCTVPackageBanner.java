package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVPackageBanner extends ContentNode implements Validatable {
	
	public CCTVPackageBanner(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/package-banner.txt");
		fillStaticMap("src/main/resources/components/cctv/package-banner-static.txt");
	}

}

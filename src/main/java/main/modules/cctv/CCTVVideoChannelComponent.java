package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVVideoChannelComponent extends ContentNode implements Validatable {
	
	public CCTVVideoChannelComponent(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/video-channel-component.txt");
	}

}

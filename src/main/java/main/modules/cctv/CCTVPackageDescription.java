package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVPackageDescription extends ContentNode implements Validatable {
	
	public CCTVPackageDescription(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/package-description.txt");
		fillStaticMap("src/main/resources/components/cctv/package-description-static.txt");
	}

}

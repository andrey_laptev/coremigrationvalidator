package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVStaticLeadTilList extends ContentNode implements Validatable {
	
	public CCTVStaticLeadTilList(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/static-lead-til-list.txt");
	}

}

package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVTidList extends ContentNode implements Validatable {
	
	public CCTVTidList(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/tid-list.txt");
	}

}

package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVShowBanner extends ContentNode implements Validatable {
	
	public CCTVShowBanner(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/show-banner.txt");
		fillStaticMap("src/main/resources/components/cctv/show-banner-static.txt");
	}

}

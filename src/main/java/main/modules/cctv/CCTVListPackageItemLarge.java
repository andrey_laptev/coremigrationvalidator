package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVListPackageItemLarge extends ContentNode implements Validatable {
	
	public CCTVListPackageItemLarge(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/list-package-item-large.txt");
	}

}

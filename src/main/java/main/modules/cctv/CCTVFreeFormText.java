package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVFreeFormText extends ContentNode implements Validatable {
	
	public CCTVFreeFormText(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/free-form-text.txt");
	}

}

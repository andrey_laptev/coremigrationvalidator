package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVMultiPromosWithGroupName extends ContentNode implements Validatable {
	
	public CCTVMultiPromosWithGroupName(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/multi-promos-with-group-name.txt");
	}


}

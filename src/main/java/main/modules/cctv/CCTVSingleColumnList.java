package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVSingleColumnList extends ContentNode implements Validatable {
	
	public CCTVSingleColumnList(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/single-column-list.txt");
	}

}

package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVPackageNav extends ContentNode implements Validatable {
	
	public CCTVPackageNav(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/package-nav.txt");
	}

}

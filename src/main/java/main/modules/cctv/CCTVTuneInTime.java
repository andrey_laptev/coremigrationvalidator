package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVTuneInTime extends ContentNode implements Validatable {
	
	public CCTVTuneInTime(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/tune-in-time.txt");
	}

}

package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVListPackageItem extends ContentNode implements Validatable {
	
	public CCTVListPackageItem(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/list-package-item.txt");
	}

}

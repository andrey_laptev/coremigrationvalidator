package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVTextImage extends ContentNode implements Validatable {
	
	public CCTVTextImage(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/text-image.txt");
	}

}

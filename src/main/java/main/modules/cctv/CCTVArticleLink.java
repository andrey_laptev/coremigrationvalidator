package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVArticleLink  extends ContentNode implements Validatable {
	
	public CCTVArticleLink(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/article-link.txt");
	}
}

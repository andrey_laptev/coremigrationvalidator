package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVTitleDescription extends ContentNode implements Validatable {
	
	public CCTVTitleDescription(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/title-description.txt");
	}

}

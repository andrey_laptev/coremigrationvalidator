package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVVideoPlayerComponent  extends ContentNode implements Validatable {
	
	public CCTVVideoPlayerComponent(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/video-player-component.txt");
	}

}

package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVQuadPod extends ContentNode implements Validatable {
	
	public CCTVQuadPod(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/quad-pod.txt");
	}

}

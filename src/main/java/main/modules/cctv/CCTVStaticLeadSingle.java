package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVStaticLeadSingle extends ContentNode implements Validatable {
	
	public CCTVStaticLeadSingle(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/static-lead-single.txt");
	}

}

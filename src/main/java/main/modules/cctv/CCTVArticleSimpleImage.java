package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVArticleSimpleImage extends ContentNode implements Validatable {
	
	public CCTVArticleSimpleImage(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/article-simple-image.txt");
	}

}

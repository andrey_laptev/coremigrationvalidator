package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVTidOne extends ContentNode implements Validatable {
	
	public CCTVTidOne(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/til-one.txt");
	}

}

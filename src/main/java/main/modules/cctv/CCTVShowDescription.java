package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVShowDescription extends ContentNode implements Validatable {
	
	public CCTVShowDescription(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/show-description.txt");
	}

}

package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVStaticLeadTilTriad extends ContentNode implements Validatable {
	
	public CCTVStaticLeadTilTriad(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/static-lead-til-triad.txt");
	}

}

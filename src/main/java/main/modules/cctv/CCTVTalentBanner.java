package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVTalentBanner extends ContentNode implements Validatable {
	
	public CCTVTalentBanner(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/talent-banner.txt");
		fillStaticMap("src/main/resources/components/cctv/talent-banner-static.txt");
	}

}

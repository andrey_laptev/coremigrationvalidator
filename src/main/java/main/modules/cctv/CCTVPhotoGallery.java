package main.modules.cctv;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class CCTVPhotoGallery extends ContentNode implements Validatable {
	
	public CCTVPhotoGallery(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/cctv/photo-gallery.txt");
	}

}

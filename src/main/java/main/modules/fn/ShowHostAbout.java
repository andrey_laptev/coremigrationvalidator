package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class ShowHostAbout extends ContentNode implements Validatable {
	
	public ShowHostAbout(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/show-host-about.txt");
		fillStaticMap("src/main/resources/components/fn/show-host-about-static.txt");
	}
	
}

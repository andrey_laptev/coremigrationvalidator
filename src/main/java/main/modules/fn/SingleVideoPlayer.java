package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class SingleVideoPlayer extends ContentNode implements Validatable {
	
	public SingleVideoPlayer(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/single-video-player.txt");
		fillStaticMap("src/main/resources/components/fn/single-video-player-static.txt");
	}
	
}

package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class FreeFormTextModule extends ContentNode implements Validatable {
	
	public FreeFormTextModule(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/free-form-text.txt");
		fillStaticMap("src/main/resources/components/fn/free-form-text-static.txt");
	}
	
}

package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class TalentPhoto extends ContentNode implements Validatable {
	
	public TalentPhoto(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/talent-photo.txt");
		fillStaticMap("src/main/resources/components/fn/talent-photo-static.txt");
	}
	
}

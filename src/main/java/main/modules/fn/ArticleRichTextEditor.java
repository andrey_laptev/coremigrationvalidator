package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class ArticleRichTextEditor extends ContentNode implements Validatable {
	
	public ArticleRichTextEditor(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/article-rich-text-editor.txt");
		fillStaticMap("src/main/resources/components/fn/article-rich-text-editor-static.txt");
	}
	
}

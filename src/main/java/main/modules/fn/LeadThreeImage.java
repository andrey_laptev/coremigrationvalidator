package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;
import main.core.Global;

public class LeadThreeImage extends ContentNode implements Validatable {
	
	public LeadThreeImage(Node node) {
		this.node = node;
		
		if ("UniversalLanding".equals(Global.getPageType()) ||
				"Show".equals(Global.getPageType())) {
			fillMap("src/main/resources/components/fn/lead-3-image-promo-collage.txt");
			fillStaticMap("src/main/resources/components/fn/lead-3-image-promo-collage-static.txt");
		} else {
			fillMap("src/main/resources/components/fn/lead-3-image.txt");
			fillStaticMap("src/main/resources/components/fn/lead-3-image-static.txt");
		}
		
	}
	
}

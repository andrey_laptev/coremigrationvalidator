package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class GenericOneVideo extends ContentNode implements Validatable {
	
	public GenericOneVideo(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/generic-1-video.txt");
		fillStaticMap("src/main/resources/components/fn/generic-1-video-static.txt");
	}
	
}

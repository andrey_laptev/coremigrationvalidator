package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class NewsletterContentWellTop extends ContentNode implements
		Validatable {
	
	public NewsletterContentWellTop(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/newsletter-content-well-top.txt");
		fillStaticMap("src/main/resources/components/fn/newsletter-content-well-top-static.txt");
	}
	
}

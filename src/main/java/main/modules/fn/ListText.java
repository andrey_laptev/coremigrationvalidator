package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class ListText extends ContentNode implements Validatable {
	
	public ListText(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/list-text.txt");
		fillStaticMap("src/main/resources/components/fn/list-text-static.txt");
	}
	
}

package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class VideoChannelModule extends ContentNode implements Validatable {
	
	public VideoChannelModule(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/video-channel-module.txt");
		fillStaticMap("src/main/resources/components/fn/video-channel-module-static.txt");
	}
	
}

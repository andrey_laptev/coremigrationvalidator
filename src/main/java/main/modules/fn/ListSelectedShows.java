package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class ListSelectedShows extends ContentNode implements Validatable {
	
	public ListSelectedShows(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/list-selected-shows.txt");
		fillStaticMap("src/main/resources/components/fn/list-selected-shows-static.txt");
	}
	
}

package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class ArticleImage extends ContentNode implements Validatable {
	
	public ArticleImage(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/article-image.txt");
		fillStaticMap("src/main/resources/components/fn/article-image-static.txt");
	}
	
}

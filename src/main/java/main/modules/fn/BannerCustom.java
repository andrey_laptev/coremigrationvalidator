package main.modules.fn;

import javax.jcr.Node;
import main.ContentNode;
import main.Validatable;

public class BannerCustom extends ContentNode implements Validatable {
	
	public BannerCustom(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/banner-custom.txt");
		fillStaticMap("src/main/resources/components/fn/banner-custom-static.txt");
	}
	
}

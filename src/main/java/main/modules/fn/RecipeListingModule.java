package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class RecipeListingModule extends ContentNode implements Validatable {
	
	public RecipeListingModule(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/recipe-listing.txt");
		fillStaticMap("src/main/resources/components/fn/recipe-listing-static.txt");
	}
}

package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class LeadThreeImageRecipe extends ContentNode implements Validatable {
	
	public LeadThreeImageRecipe(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/lead-3-image-recipe.txt");
		fillStaticMap("src/main/resources/components/fn/lead-3-image-recipe-static.txt");
	}
	
}

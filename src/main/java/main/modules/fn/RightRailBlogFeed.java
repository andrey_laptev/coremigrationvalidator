package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class RightRailBlogFeed extends ContentNode implements Validatable {
	
	public RightRailBlogFeed(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/right-rail-blog-feed.txt");
		fillStaticMap("src/main/resources/components/fn/right-rail-blog-feed-static.txt");
	}
	
}

package main.modules.fn;

import javax.jcr.Node;
import main.ContentNode;
import main.Validatable;

public class CarouselPhotoGalleryPromo extends ContentNode implements
		Validatable {
	
	public CarouselPhotoGalleryPromo(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/carousel-photo-gallery-promo.txt");
		fillStaticMap("src/main/resources/components/fn/carousel-photo-gallery-promo-static.txt");
	}
	
}

package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class ListRecipeText extends ContentNode implements Validatable {
	
	public ListRecipeText(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/list-recipe-text.txt");
		fillStaticMap("src/main/resources/components/fn/list-recipe-text-static.txt");
	}
	
}

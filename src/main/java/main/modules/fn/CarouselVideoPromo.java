package main.modules.fn;

import javax.jcr.Node;
import main.ContentNode;
import main.Validatable;

public class CarouselVideoPromo extends ContentNode implements Validatable {
	
	public CarouselVideoPromo(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/carousel-video-promo.txt");
		fillStaticMap("src/main/resources/components/fn/carousel-video-promo-static.txt");
	}
	
}

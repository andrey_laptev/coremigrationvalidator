package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class ListTalentShows extends ContentNode implements Validatable {
	
	public ListTalentShows(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/list-talent-shows.txt");
		fillStaticMap("src/main/resources/components/fn/list-talent-shows-static.txt");
	}
	
}

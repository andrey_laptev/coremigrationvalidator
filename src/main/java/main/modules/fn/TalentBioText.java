package main.modules.fn;

import javax.jcr.Node;

import main.ContentNode;
import main.Validatable;

public class TalentBioText extends ContentNode implements Validatable {
	
	public TalentBioText(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/talent-bio-text.txt");
		fillStaticMap("src/main/resources/components/fn/talent-bio-text-static.txt");
	}
	
}

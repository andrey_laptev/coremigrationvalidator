package main.modules.fn;

import javax.jcr.Node;
import main.ContentNode;
import main.Validatable;

public class GenericOneImage extends ContentNode implements Validatable {
	
	public GenericOneImage(Node node) {
		this.node = node;
		fillMap("src/main/resources/components/fn/generic-1-image.txt");
		fillStaticMap("src/main/resources/components/fn/generic-1-image-static.txt");
	}
	
}

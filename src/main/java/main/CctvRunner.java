package main;

import main.core.Config;
import main.modules.cctv.*;
import main.pages.cctv.*;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;


public class CctvRunner {

    public static Validatable getValidatable(Node node)
            throws ValueFormatException, RepositoryException {

        if (!node.hasProperty("sling:resourceType")){
            return null;
        }

        final String cctvArticlePage = "sni-wcm/components/pagetypes/article-simple";
        final String cctvBioPage = "sni-wcm/components/pagetypes/bio";
        final String cctvChannelLandingPage = "sni-wcm/components/pagetypes/package-landing";
        final String cctvEpisodePage = "sni-wcm/components/pagetypes/episode";
        final String cctvEpisodeArchivePage = "sni-wcm/components/pagetypes/episode-archive";
        final String cctvFreeFormTextPage = "sni-wcm/components/pagetypes/free-form-text";
        final String cctvIndexPage = "sni-wcm/components/pagetypes/index";
        final String cctvPhotoGalleryPage = "sni-wcm/components/pagetypes/photo-gallery";
        final String cctvRecipePage = "sni-wcm/components/pagetypes/recipe";
        final String cctvSeriePage = "sni-wcm/components/pagetypes/series";
        final String cctvShowPage = "sni-wcm/components/pagetypes/show";
        final String cctvShowRecipesPage = "sni-wcm/components/pagetypes/show-recipes";
        final String cctvTalentRecipePage = "sni-wcm/components/pagetypes/talent-recipes";
        final String cctvTalentPage = "sni-wcm/components/pagetypes/talent";
        final String cctvTopicPage = "sni-wcm/components/pagetypes/topic";
        final String cctvVideoPage = "sni-wcm/components/pagetypes/video";
        final String cctvVideoChannelPage = "sni-wcm/components/pagetypes/video-channel";
        final String cctvVideoPlayerPage = "sni-wcm/components/pagetypes/video-player";

        //modules
        final String cctvArticleSimpleTitle = "sni-wcm/components/pagetypes/article-simple/components/article-simple-title";
        final String cctvArticleSimpleImage = "sni-wcm/components/pagetypes/article-simple/components/article-simple-image";
        final String cctvArticleSimpleText = "sni-wcm/components/pagetypes/article-simple/components/article-simple-text";
        final String cctvArticleLink = "sni-wcm/components/modules/article-link";
        final String cctvPackageNav = "sni-wcm/components/pagetypes/package-landing/components/package-nav";
        final String cctvThumbnailGrid = "sni-wcm/components/modules/thumbnail_grid";
        //final String cctvShowBanner = "sni-wcm/components/pagetypes/show/components/show-banner";
        final String cctvTuneInTime = "sni-wcm/components/modules/tune-in-time";
        final String cctvListPackageItemLarge = "sni-wcm/components/modules/list-package/components/list-package-item-large";
        final String cctvListPackageItem = "sni-wcm/components/modules/list-package/components/list-package-item";
        final String cctvStaticLeadSingle = "sni-wcm/components/modules/static-lead-single";
        final String cctvMultiPromosWithGroupName = "sni-wcm/components/modules/multi-promos-with-group-name";
        final String cctvSingleColumnList = "sni-wcm/components/modules/single-column-list";
        final String cctvFreeFormText = "sni-wcm/components/pagetypes/free-form-text/components/free-form-text";
        final String cctvStaticLeadTilList = "sni-wcm/components/modules/static-lead-til-list";
        final String cctvVideoChannelComponent = "sni-wcm/components/pagetypes/video-channel/components/video-channel-component";
        final String cctvPackageDescription = "sni-wcm/components/pagetypes/package-landing/components/package-description";
        final String cctvTextImage = "sni-wcm/components/pagetypes/bio/components/text-image";
        final String cctvQuadPod = "sni-wcm/components/modules/quad-pod";
        final String cctvTitleDescription = "sni-wcm/components/pagetypes/photo-gallery/components/title-description";
        final String cctvPhotoGallery = "sni-wcm/components/pagetypes/photo-gallery/components/photo-gallery";
        final String cctvTidList = "sni-wcm/components/modules/tid-list";
        final String cctvTidOne = "sni-wcm/components/modules/til-one";
        final String cctvStaticLeadTilTriad = "sni-wcm/components/modules/static-lead-til-triad";
        final String cctvTextArea = "sni-wcm/components/modules/text-area";
        final String cctvPackageBanner = "sni-wcm/components/pagetypes/package-landing/components/package-banner";
        final String cctvShowDescription = "sni-wcm/components/pagetypes/show/components/show-description";
        final String cctvTalentBanner = "sni-wcm/components/pagetypes/talent/components/talent-banner";
        final String cctvListPackage = "sni-wcm/components/modules/list-package";
        final String cctvTalentDescription = "sni-wcm/components/pagetypes/talent/components/talent-description";
        final String cctvVideoPlayerComponent = "sni-wcm/components/pagetypes/video-player/components/video-player-component";
        final String cctvPrimaryTalent = "sni-wcm/components/pagetypes/show/components/primary-talent";
        final String cctvTopic = "cookcom/components/article/topic";

        String resourceType = node.getProperty("sling:resourceType").getString();

        //cctv
        if (cctvArticlePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("article") ? new CCTVArticlePage(node) : null;
        }
        if (cctvBioPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("bio") ? new CCTVBioPage(node) : null;
        }
        if (cctvChannelLandingPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("package-landing") ? new CCTVChannelLandingPage(node) : null;
        }
        if (cctvEpisodeArchivePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("episode-archive") ? new CCTVEpisodeArchivePage(node) : null;
        }
        if (cctvEpisodePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("episode") ? new CCTVEpisodePage(node) : null;
        }
        if (cctvFreeFormTextPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("free-form-text") ? new CCTVFreeFormTextPage(node) : null;
        }
        if (cctvIndexPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("index") ? new CCTVIndexPage(node) : null;
        }
        if (cctvRecipePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("recipe") ? new CCTVRecipePage(node) : null;
        }
        if (cctvPhotoGalleryPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("photo-gallery") ? new CCTVPhotoGalleryPage(node) : null;
        }
        if (cctvSeriePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("series") ? new CCTVSeriePage(node) : null;
        }
        if (cctvShowPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("show") ? new CCTVShowPage(node) : null;
        }
        if (cctvShowRecipesPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("show-recipes") ? new CCTVShowRecipesPage(node) : null;
        }
        if (cctvTopicPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("topic") ? new CCTVTopicPage(node) : null;
        }
        if (cctvTalentPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("talent") ? new CCTVTalentPage(node) : null;
        }
        if (cctvTalentRecipePage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("talent-recipes") ? new CCTVTalentRecipePage(node) : null;
        }
        if (cctvVideoPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("video") ? new CCTVVideoPage(node) : null;
        }
        if (cctvVideoChannelPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("video-channel") ? new CCTVVideoChannelPage(node) : null;
        }
        if (cctvVideoPlayerPage.equalsIgnoreCase(resourceType)){
            return Config.isValidPage("video-player") ? new CCTVVideoPlayerPage(node) : null;
        }
        //cctv
        if (cctvArticleLink.equalsIgnoreCase(resourceType)){
            return new CCTVArticleLink(node);
        }
        if (cctvArticleSimpleImage.equalsIgnoreCase(resourceType)){
            return new CCTVArticleSimpleImage(node);
        }
        if (cctvArticleSimpleText.equalsIgnoreCase(resourceType)){
            return new CCTVArticleSimpleText(node);
        }
        if (cctvArticleSimpleTitle.equalsIgnoreCase(resourceType)){
            return new CCTVArticleSimpleTitle(node);
        }
        if (cctvFreeFormText.equalsIgnoreCase(resourceType)){
            return new CCTVFreeFormText(node);
        }
        if (cctvListPackage.equalsIgnoreCase(resourceType)){
            return new CCTVListPackage(node);
        }
        if (cctvListPackageItem.equalsIgnoreCase(resourceType)){
            return new CCTVListPackageItem(node);
        }
        if (cctvListPackageItemLarge.equalsIgnoreCase(resourceType)){
            return new CCTVListPackageItemLarge(node);
        }
        if (cctvMultiPromosWithGroupName.equalsIgnoreCase(resourceType)){
            return new CCTVMultiPromosWithGroupName(node);
        }
        if (cctvPackageBanner.equalsIgnoreCase(resourceType)){
            return new CCTVPackageBanner(node);
        }
        if (cctvPackageDescription.equalsIgnoreCase(resourceType)){
            return new CCTVMultiPromosWithGroupName(node);
        }
        if (cctvPackageNav.equalsIgnoreCase(resourceType)){
            return new CCTVPackageNav(node);
        }
        if (cctvPhotoGallery.equalsIgnoreCase(resourceType)){
            return new CCTVPhotoGallery(node);
        }
        if (cctvPrimaryTalent.equalsIgnoreCase(resourceType)){
            return new CCTVPrimaryTalent(node);
        }
        if (cctvQuadPod.equalsIgnoreCase(resourceType)){
            return new CCTVQuadPod(node);
        }
			/* Mapping rules updated
			if (cctvShowBanner.equalsIgnoreCase(resourceType)){
				return new CCTVShowBanner(node);
			}
			*/
        if (cctvShowDescription.equalsIgnoreCase(resourceType)){
            return new CCTVShowDescription(node);
        }
        if (cctvSingleColumnList.equalsIgnoreCase(resourceType)){
            return new CCTVSingleColumnList(node);
        }
        if (cctvStaticLeadSingle.equalsIgnoreCase(resourceType)){
            return new CCTVStaticLeadSingle(node);
        }
        if (cctvTextArea.equalsIgnoreCase(resourceType)){
            return new CCTVTextArea(node);
        }
        if (cctvStaticLeadTilList.equalsIgnoreCase(resourceType)){
            return new CCTVStaticLeadTilList(node);
        }
        if (cctvStaticLeadTilTriad.equalsIgnoreCase(resourceType)){
            return new CCTVStaticLeadTilTriad(node);
        }
        if (cctvTalentBanner.equalsIgnoreCase(resourceType)){
            return new CCTVTalentBanner(node);
        }
        if (cctvTalentDescription.equalsIgnoreCase(resourceType)){
            return new CCTVTalentDescription(node);
        }
        if (cctvTextImage.equalsIgnoreCase(resourceType)){
            return new CCTVTextImage(node);
        }
        if (cctvThumbnailGrid.equalsIgnoreCase(resourceType)){
            return new CCTVThumbnailGrid(node);
        }
        if (cctvTidList.equalsIgnoreCase(resourceType)){
            return new CCTVTidList(node);
        }
        if (cctvTidOne.equalsIgnoreCase(resourceType)){
            return new CCTVTidOne(node);
        }
        if (cctvTitleDescription.equalsIgnoreCase(resourceType)){
            return new CCTVTitleDescription(node);
        }
        if (cctvTopic.equalsIgnoreCase(resourceType)){
            return new CCTVTopic(node);
        }
        if (cctvTuneInTime.equalsIgnoreCase(resourceType)){
            return new CCTVTuneInTime(node);
        }
        if (cctvVideoChannelComponent.equalsIgnoreCase(resourceType)){
            return new CCTVVideoChannelComponent(node);
        }
        if (cctvVideoPlayerComponent.equalsIgnoreCase(resourceType)){
            return new CCTVVideoPlayerComponent(node);
        }

        return null;
    }
}

package main;

import main.core.Config;
import main.modules.fn.*;
import main.pages.fn.*;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;


public class FnRunner {

    public static Validatable getValidatable(Node node)
            throws ValueFormatException, RepositoryException {

        if (!node.hasProperty("sling:resourceType")){
            return null;
        }
            // fn
        final String show = "sni-food/components/pagetypes/show";
        final String episodeListing = "sni-food/components/pagetypes/episode-listing";
        final String company = "sni-food/components/pagetypes/company";
        final String article = "sni-food/components/pagetypes/article-simple";
        final String assetRecipes = "sni-food/components/pagetypes/asset-recipes";
        final String bio = "sni-food/components/pagetypes/bio";
        final String freeFormText = "sni-food/components/pagetypes/free-form-text";
        final String menu = "sni-food/components/pagetypes/menu";
        final String menuListing = "sni-food/components/pagetypes/menu-listing";
        final String photoGallery = "sni-food/components/pagetypes/photo-gallery";
        final String photoGalleryListing = "sni-food/components/pagetypes/photo-gallery-listing";
        final String recipe = "sni-food/components/pagetypes/recipe";
        final String recipeListing = "sni-food/components/pagetypes/recipe-listing";
        final String series = "sni-food/components/pagetypes/series";
        final String singleVideo = "sni-food/components/pagetypes/video";
        final String structural = "sni-core/components/util/structural-page";
        final String talent = "sni-food/components/pagetypes/talent";
        final String topic = "sni-food/components/pagetypes/topic";
        final String universalLanding = "sni-food/components/pagetypes/universal-landing";
        final String videoChannel = "sni-food/components/pagetypes/video-channel";
        final String videoPlayer = "sni-food/components/pagetypes/video-player";
        final String episode = "sni-food/components/pagetypes/episode";

        final String secondaryGrid = "sni-food/components/modules/secondary-grid";
        final String secondaryBottomPromo = "sni-food/components/modules/secondary-bottom-promo";
        final String bannerCusom = "sni-food/components/modules/banner-custom";
        final String bannerSimple = "sni-food/components/modules/banner-simple";
        final String carouselPhotoGalleryPromo = "sni-food/components/modules/carousel-photo-gallery-promo";
        final String carouselVideoPromo = "sni-food/components/modules/carousel-video-promo";
        final String genericOneImage = "sni-food/components/modules/generic-1-image";
        final String articleImage = "sni-food/components/modules/article-image";
        final String freeFormTextModule = "sni-food/components/modules/free-form-text";
        final String genericOneVideo = "sni-food/components/modules/generic-1-video";
        final String leadThreeImage = "sni-food/components/modules/lead-3-image";
        final String listSelectedShows = "sni-food/components/modules/list-selected-shows";
        final String listTalentShows = "sni-food/components/modules/list-talent-shows";
        final String listText = "sni-food/components/modules/list-text";
        final String newsletterContentWellTop = "sni-food/components/modules/newsletter-content-well-top";
        final String rightRailBlogFeed = "sni-food/components/modules/right-rail-blog-feed";
        final String showHostAbout = "sni-food/components/modules/show-host-about";
        final String singleVideoPlayer = "sni-food/components/modules/single-video-player";
        final String talentBioText = "sni-food/components/modules/talent-bio-text";
        final String talentPhoto = "sni-food/components/modules/talent-photo";
        final String videoChannelModule = "sni-food/components/modules/video-channel-module";
        final String articleRichTextEditor = "sni-food/components/pagetypes/article-simple/components/rich-text-editor";
        final String leadThreeImageRecipe = "sni-food/components/modules/lead-3-image-recipe";
        final String listRecipeText = "sni-food/components/modules/list-recipe-text";
        final String recipeListingModule = "sni-food/components/modules/recipe-listing";


        String resourceType = node.getProperty("sling:resourceType").getString();
        // Pages
        //fn
        if (show.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("show") ? new Show(node) : null;
        }
        if (company.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("company") ? new Company(node) : null;
        }
        if (episodeListing.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("episode-listing") ? new EpisodeListing(node) : null;
        }
        if (article.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("article") ? new Article(node) : null;
        }
        if (assetRecipes.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("asset-recipes") ? new AssetRecipes(node) : null;
        }
        if (bio.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("bio") ? new Bio(node) : null;
        }
        if (freeFormText.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("free-form-text") ? new FreeFormText(node) : null;
        }
        if (menu.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("menu") ? new Menu(node) : null;
        }
        if (menuListing.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("menu-listing") ? new MenuListing(node) : null;
        }
        if (photoGallery.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("photo-gallery") ? new PhotoGallery(node) : null;
        }
        if (photoGalleryListing.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("photo-gallery-listing") ? new PhotoGalleryListing(node) : null;
        }
        if (recipe.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("recipe") ? new Recipe(node) : null;
        }
        if (recipeListing.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("recipe-listing") ? new RecipeListing(node) : null;
        }
        if (series.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("series") ? new Series(node) : null;
        }
        if (singleVideo.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("video") ? new SingleVideo(node) : null;
        }
        if (structural.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("structural") ? new Structural(node) : null;
        }
        if (talent.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("talent") ? new Talent(node) : null;
        }
        if (topic.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("topic") ? new Topic(node) : null;
        }
        if (universalLanding.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("ulp") ? new UniversalLanding(node) : null;
        }
        if (videoChannel.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("video-channel") ? new VideoChannel(node) : null;
        }
        if (videoPlayer.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("video-player") ? new VideoPlayer(node) : null;
        }
        if (episode.equalsIgnoreCase(resourceType)) {
            return Config.isValidPage("episode") ? new Episode(node) : null;
        }

        // Modules
        //fn
        if (secondaryGrid.equalsIgnoreCase(resourceType)) {

        }
        if (secondaryBottomPromo.equalsIgnoreCase(resourceType)) {

        }
        if (bannerCusom.equalsIgnoreCase(resourceType)) {
            return new BannerCustom(node);
        }
        if (bannerSimple.equalsIgnoreCase(resourceType)) {
            return new BannerSimple(node);
        }
        if (carouselPhotoGalleryPromo.equalsIgnoreCase(resourceType)) {
            return new CarouselPhotoGalleryPromo(node);
        }
        if (carouselVideoPromo.equalsIgnoreCase(resourceType)) {
            return new CarouselVideoPromo(node);
        }
        if (genericOneImage.equalsIgnoreCase(resourceType)) {
            return new GenericOneImage(node);
        }
        if (articleImage.equalsIgnoreCase(resourceType)) {
            return new ArticleImage(node);
        }
        if (freeFormTextModule.equalsIgnoreCase(resourceType)) {
            return new FreeFormTextModule(node);
        }
        if (genericOneVideo.equalsIgnoreCase(resourceType)) {
            return new GenericOneVideo(node);
        }
        if (leadThreeImage.equalsIgnoreCase(resourceType)) {
            return new LeadThreeImage(node);
        }
        if (listSelectedShows.equalsIgnoreCase(resourceType)) {
            return new ListSelectedShows(node);
        }
        if (listTalentShows.equalsIgnoreCase(resourceType)) {
            return new ListTalentShows(node);
        }
        if (listText.equalsIgnoreCase(resourceType)) {
            return new ListText(node);
        }
        if (newsletterContentWellTop.equalsIgnoreCase(resourceType)) {
            return new NewsletterContentWellTop(node);
        }
        if (rightRailBlogFeed.equalsIgnoreCase(resourceType)) {
            return new RightRailBlogFeed(node);
        }
        if (showHostAbout.equalsIgnoreCase(resourceType)) {
            return new ShowHostAbout(node);
        }
        if (singleVideoPlayer.equalsIgnoreCase(resourceType)) {
            return new SingleVideoPlayer(node);
        }
        if (talentBioText.equalsIgnoreCase(resourceType)) {
            return new TalentBioText(node);
        }
        if (talentPhoto.equalsIgnoreCase(resourceType)) {
            return new TalentPhoto(node);
        }
        if (videoChannelModule.equalsIgnoreCase(resourceType)) {
            return new VideoChannelModule(node);
        }
        if (articleRichTextEditor.equalsIgnoreCase(resourceType)) {
            return new ArticleRichTextEditor(node);
        }
        if (leadThreeImageRecipe.equalsIgnoreCase(resourceType)) {
            return new LeadThreeImageRecipe(node);
        }
        if (listRecipeText.equalsIgnoreCase(resourceType)) {
            return new ListRecipeText(node);
        }
        if (recipeListingModule.equalsIgnoreCase(resourceType)) {
            return new RecipeListingModule(node);
        }

        return null;
    }

}
